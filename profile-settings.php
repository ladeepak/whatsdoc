<?php include('header.php'); ?>
<?php //echo "<pre>"; print_r($_SESSION); echo  "</pre>";?>
 <?php  if(!isset($_SESSION['wsKey'])) 
    {
   	 ?>   	 
<script>
	   window.location  ='index.php' ;
</script>
 <?php 
   } 
   ?>
   <?php 
   $patient_id=$_SESSION['data']->patient->id;
   $wskey=$_SESSION['data']->patient->wsKey;
   $loginUrl =  $baseUrl.'patient/details';
		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'patientId='.$patient_id);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Ws-Key: $wskey"
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$store = curl_exec($ch);

		$data =  json_decode($store );
		//echo '<pre>';print_r($data); echo '</pre>';

		curl_close($ch);
	
   ?>
   
   <script>
   
   
   $(document).ready(function(){
	$("#update_profile_btn").click(function(){
	
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	$("#firstname").focusout(function(){
	     var dest = $(this);
        dest.val(dest.val().split(" ").join(""));  
	});
	
	
	if($.trim($('#f_name').val()) == ""){
		var firstname = $("#f_name");
		$('#f_name').attr('placeholder','Please enter firstname');
		$('#f_name').attr('style','border-color:red');
		ScrollToTop(firstname);
		return false;
		
	}else if($.trim($('#l_name').val()) == ""){
		var lastname = $("#l_name");
		$('#l_name').attr('placeholder','Please enter lastname');
		$('#l_name').attr('style','border-color:red');
		ScrollToTop(lastname);
		return false;
		
	}else if($.trim($('#smobile').val()) == ""){
		var mobile = $("#smobile");
		$('#smobile').attr('placeholder','Please enter mobile');
		$('#smobile').attr('style','border-color:red');
		ScrollToTop(mobile);
		return false;
		
	}else if($.trim($('#semail').val()) == ""){
		var email = $("#semail");
		$('#semail').attr('placeholder','Please enter email');
		$('#semail').attr('style','border-color:red');
		ScrollToTop(email);
		return false;
		
	}else if(!regex.test($.trim($('#semail').val()))){
		var email = $("#semail");
		$('#semail').val('');
		$('#semail').attr('placeholder','Please enter a valid email');
		$('#semail').attr('style','border-color:red');
		ScrollToTop(email);
		return false;
		
	}else if($.trim($('#dob_day_settings').val()) == 0){
		var days = $("#dob_day_settings");
		$('#dob_day_settings').attr('style','border-color:red;');
		ScrollToTop(days);
		return false;
		
	}else if($.trim($('#dob_month_settings').val()) == 0){
		var months = $("#dob_month_settings");
		$('#dob_month_settings').attr('style','border-color:red;');
		ScrollToTop(months);
		return false;
		
	}else if($.trim($('#dob_year_settings').val()) == 0){
		var months = $("#dob_year_settings");
		$('#dob_year_settings').attr('style','border-color:red;');
		ScrollToTop(years);
		return false;
		
	} else {
		
			var data = $('#update_profile').serialize()+'&'+$.param({  'action' : 'update_profile' });
			$.ajax({
				url: "doctors.php",
				type: 'POST',
				data: data ,
				dataType: "json",
				success: function(data) {
				//	alert(data);
				   if(data.response =='Success'){
					   alert('Data is successfully updated.');
					   location.reload();
				   } else if(data.response =='Error') {
					   
					   alert(data.message);
				   } else {
						alert('There is some problem in system,please try later.');
				   }
			   }
			});
			return false;
		}
	});


  /**
  change password
  */
  $("#update_password_btn").click(function(){
	   if($.trim($('#currentpassword').val()) == ""){
			$('#currentpassword').attr('placeholder','Please enter current password');
			$('#currentpassword').attr('style','border-color:red');
			return false;
		}else if($.trim($('#newpassword').val()) == ""){
			$('#newpassword').attr('placeholder','Please enter new password');
			$('#newpassword').attr('style','border-color:red');
			return false;
		}else if($('#newpassword').val() != $('#con_newpassword').val()){
			$('#newpassword').val('');
			$('#con_newpassword').val('');
			$('#newpassword').attr('placeholder','Passwords not confirmed');
			$('#newpassword').attr('style','border-color:red');
			//$('#confirm_password').attr('placeholder','Password doesnot match with confirm password');
			$('#con_newpassword').attr('style','border-color:red');
			return false;
		} else {
			
				var data = $('#update_password').serialize()+'&'+$.param({  'action' : 'update_password' });
				$.ajax({
					url: "doctors.php",
					type: 'POST',
					data: data ,
					dataType: "json",
					success: function(data) {
						alert(data);
						   if(data.response =='Success'){
							   alert('Password updated successfully');
							   location.reload();
						   } else if(data.response =='Error') {
							   
							   alert(data.message);
						   } else {
								alert('There is some problem in system,please try later.');
						   }
					   }
				});
				return false;
				
		}
	});
});

   </script>
   
<div class="inner-search-bar">
 <div class="container">
  <h1>Profile Settings</h1>
 </div>
</div>

<div class="profile-detail-doc">
 <div class="container"> 
   <div class="profile-settings">
      <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Genral Information</a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Password Settings</a></li>
  </ul>  
    <div class="tab-content pro-setis">
  
		<div role="tabpanel" class="tab-pane in active" id="messages">

  <form  action="" method="POST" id="update_profile">
     
      
			<div class="row">
			  <label>Name</label>
			 <div class="col-lg-6 no-pds">
			  <input type="text" id="f_name" placeholder="First Name" name="firstname" value="<?php echo $data->name->givenName ; ?>">
			  <input type="hidden" name="wskey" value="<?php echo $data->wsKey ?>">
			  <input type="hidden" name="patient_id" value="<?php echo $data->id ?>">
			 </div>
			 <div class="col-lg-6 no-pds">
			  <input type="text" id="l_name" placeholder="Last Name" name="lastname" value="<?php echo $data->name->familyName ; ?>">
			 </div>
		   
			</div>
		  		  
		  <div class="row">
		   <label>Mobile Number</label>
		   <div class="col-lg-12 no-pds">
			<input type="text"  id="smobile" placeholder="Enter Mobile" name="mobile" value="<?php echo $data->homeTelephone->number ; ?>">
		   </div>		   
		  </div>
		  
		  
		  <div class="row">
		   <label>Email</label>
		   <div class="col-lg-12 no-pds">
			<input type="email" id="semail" placeholder="Enter Email" name="email" value="<?php echo $data->homeAddress->onlineEmail ; ?>">
		   </div>
		   
		  </div>

		  <div class="row">
		   <label>Date of Birth</label>
			 <?php
				$seconds1 = $data->birthDate / 1000;
				 $year = date("Y", $seconds1); 
				 $month = date("m", $seconds1); 
				 $date = date("d", $seconds1); 
			 ?>
		   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pds">
			<select name="dob_day" id="dob_day_settings">
			<?php if( $data->birthDate !='') { ?>
				<option selected value="<?php echo $date ; ?>"><?php echo $date ; ?></option>
			<?php } ?>	
			<option value="0">-</option>
			<option value="01">1</option>
			<option value="02">2</option>
			<option value="03">3</option>
			<option value="04">4</option>
			<option value="05">5</option>
			<option value="06">6</option>
			<option value="07">7</option>
			<option value="08">8</option>
			<option value="09">9</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
			<option value="13">13</option>
			<option value="14">14</option>
			<option value="15">15</option>
			<option value="16">16</option>
			<option value="17">17</option>
			<option value="18">18</option>
			<option value="19">19</option>
			<option value="20">20</option>
			<option value="21">21</option>
			<option value="22">22</option>
			<option value="23">23</option>
			<option value="24">24</option>
			<option value="25">25</option>
			<option value="26">26</option>
			<option value="27">27</option>
			<option value="28">28</option>
			<option value="29">29</option>
			<option value="30">30</option>
			<option value="31">31</option>
		</select>
		   </div>
		   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pds">
			<select name="dob_month" id="dob_month_settings">
			<?php if( $data->birthDate !='') { ?>
				<option selected value="<?php echo $month ; ?>"><?php echo $month ; ?></option>
			<?php } ?>	
				<option value="0">-</option>
				<option value="1">January</option>
				<option value="2">Febuary</option>
				<option value="3">March</option>
				<option value="4">April</option>
				<option value="5">May</option>
				<option value="6">June</option>
				<option value="7">July</option>
				<option value="8">August</option>
				<option value="9">September</option>
				<option value="10">October</option>
				<option value="11">November</option>
				<option value="12">December</option>
		   </select>
		   </div>
		   
		   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pds">
			 <select name="dob_year" id="dob_year_settings">
			 <?php if( $data->birthDate !='') { ?>
				<option selected value="<?php echo $year ; ?>"><?php echo $year ; ?></option>
			<?php } ?>	
			<option value="0">-</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009">2009</option>
			<option value="2008">2008</option>
			<option value="2007">2007</option>
			<option value="2006">2006</option>
			<option value="2005">2005</option>
			<option value="2004">2004</option>
			<option value="2003">2003</option>
			<option value="2002">2002</option>
			<option value="2001">2001</option>
			<option value="2000">2000</option>
			<option value="1999">1999</option>
			<option value="1998">1998</option>
			<option value="1997">1997</option>
			<option value="1996">1996</option>
			<option value="1995">1995</option>
			<option value="1994">1994</option>
			<option value="1993">1993</option>
			<option value="1992">1992</option>
			<option value="1991">1991</option>
			<option value="1990">1990</option>
			<option value="1989">1989</option>
			<option value="1988">1988</option>
			<option value="1987">1987</option>
			<option value="1986">1986</option>
			<option value="1985">1985</option>
			<option value="1984">1984</option>
			<option value="1983">1983</option>
			<option value="1982">1982</option>
			<option value="1981">1981</option>
			<option value="1980">1980</option>
			<option value="1979">1979</option>
			<option value="1978">1978</option>
			<option value="1977">1977</option>
			<option value="1976">1976</option>
			<option value="1975">1975</option>
			<option value="1974">1974</option>
			<option value="1973">1973</option>
			<option value="1972">1972</option>
			<option value="1971">1971</option>
			<option value="1970">1970</option>
			<option value="1969">1969</option>
			<option value="1968">1968</option>
			<option value="1967">1967</option>
			<option value="1966">1966</option>
			<option value="1965">1965</option>
			<option value="1964">1964</option>
			<option value="1963">1963</option>
			<option value="1962">1962</option>
			<option value="1961">1961</option>
			<option value="1960">1960</option>
			<option value="1959">1959</option>
		</select>
		   </div>
		   
		  </div>
		  
		  
		   <div class="row">
		   <label>Sex</label>
		   <div class="col-lg-6 no-pds">
			<select name="gender" id="gender">
			 <option <?php echo $data->gender =='M' ? 'selected="selected"' : '' ;?> value="M">Male</option>
			 <option <?php echo $data->gender =='F' ? 'selected="selected"' : '' ;?>  value="F">Female</option>
			</select>
		   </div>

		   
		  </div>
		     <!--[emailNotification] => 1
                    [pushNotification] => 1
                    [textMessages] => 1-->
                    
           <div class="row">
		   <label>Emails Notification</label>
		   <div class="col-lg-6 no-pds">
			<input type="checkbox" name="email_notify" id="email_notify" <?php if(isset($data->emailNotification) && ($data->emailNotification == 1)){ echo 'checked';}?>> 
		   </div>

		   
		  </div>
		   
           <div class="row">
		   <label>Text Messages Notifications</label>
		   <div class="col-lg-6 no-pds">
			<input type="checkbox" name="text_notify" id="text_notify" <?php if(isset($data->textMessages) && ($data->textMessages == 1)){ echo 'checked';}?>> 
		   </div>

		   
		  </div>
		   
           <div class="row">
		   <label>Push Notifications</label>
		   <div class="col-lg-6 no-pds">
			<input type="checkbox" name="push_notify" id="push_notify" <?php if(isset($data->pushNotification) && ($data->pushNotification == 1)){ echo 'checked';}?>> 
		   </div>

		   
		  </div>
		   
        
			<div class="row">
			 <button id="update_profile_btn">Save Changes</button>
			</div>
			  
  
    </form>
    		   
	
		
		
		</div>
		<div role="tabpanel" class="tab-pane" id="settings">
		
	
		  <form  action="" method="POST" id="update_password">
	  

				
				
				<div class="row">
				 <label>Current Password</label>
				<div class="col-lg-12 no-pds">
				 <input type="password" id="currentpassword" placeholder="Current Password" name="currentpassword" value="">
				   <input type="hidden" name="wskey" value="<?php echo $data->wsKey ?>">
			      <input type="hidden" name="patient_id" value="<?php echo $data->id ?>">
			
				</div>

				</div>
				
				<div class="row">
				 <label>New Password</label>
				<div class="col-lg-12 no-pds">
				 <input type="password" placeholder="New Password" name="newpassword" id="newpassword" value="">
				</div>

				</div>
				
				<div class="row">
				 <label>Confirm Password</label>
				<div class="col-lg-12 no-pds">
				 <input type="password" placeholder="Confirm Password" name="con_newpassword" id="con_newpassword" value="">
				</div>

				</div> 
				
				<div class="row">
				 <button id="update_password_btn">Save Changes</button>
				</div>
          </form
		
		</div>
  </div>
   

   
   
   </div>
   
   
 </div>
</div>
<?php include('footer.php'); ?>
