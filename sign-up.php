<?php include('header.php'); ?>

<?php 
if(isset($_SESSION['wsKey'])) 
 {
	 ?>
	 <script>
	 window.location  ='index.php' ;
	 </script>
	 <?php 
 } 
?>
<div class="inner-search-bar">
 <div class="container">
  
  <h1>Sign Up</h1>
  
  
 </div>
</div>

<div class="outer-user">

<div class="container">
	<?php if(isset($_GET['physicianid']) && (!empty($_GET['physicianid']))){ 
	//echo $_GET['physicianid']; 	
	$loginUrl = $baseUrl.'physician/get_by_id_guest_visible';
   
   
   //init curl
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $loginUrl);
   curl_setopt($ch, CURLOPT_POST, 1);
   curl_setopt($ch, CURLOPT_POSTFIELDS, 'physicianId='.$physicianid);
   
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $store = curl_exec($ch);
   
   $dataPhy =  json_decode($store);
   	//echo '<pre>'; print_r($dataPhy) ; echo '</pre>';
   curl_close($ch);
	?>
	<div class="doctor-appoitn">
		<div class="doc-names">
			<table width="100%" border="0">
				<tr>
					<td>
						<?php if(!empty($dataPhy->image)){
							 // var_dump(file_exists($baseUrl.'images/'.$data->image));
							 if(file_exists($baseUrl.'images/'.$dataPhy->image == true)){?>
								<img alt="Doctor" src="<?php echo $baseUrl.'images/'.$dataPhy->image;?>">	
							 <?php
							 }else{ ?>
								 <img alt="Doctor" src="<?php echo 'images/demouser.jpg';?>"> 
							 
						  
							 <?php } }else{ ?>
						  <img alt="Doctor" src="<?php echo 'images/demouser.jpg';?>">
						  <?php } ?>	
						<!--img src="images/doctor-av.jpg" alt="doc" /-->
					</td>
					<td valign="top">
						<h4><?php echo $dataPhy->prefix.' '.$dataPhy->givenName.' '.$dataPhy->familyName?></h4>
						<div class="dc-rating">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
						</div>
						
						<p><?php echo $dataPhy->defaultLocation->address->representation;?></p>
					</td>
				</tr>
			</table>
			
			<!--h5>Thursday, February 25 - 9:45 AM</h5>
			<p>Patient</p>
			<span>TBD</span>
			<p>Reason for Visit</p>
			<span>illness</span-->
		</div>
		<p class="secrue"><i class="fa fa-lock"></i> Secure Booking</p>
	</div>
 <?php }?>
 
 
 <div class="sign-up">

 <div class="hedings">
  <h5>Create an account</h5>
  <p>This will help you manage your appointments</p>
 </div>
 <script>

</script>
 <form id="form_signup" method="POST" action="" >
  <div class="row">
   <label>Name</label>
   <div class="col-lg-6 no-pds">
    <input type="text" value="" id="firstname" name="firstname" placeholder="First Name" onkeyup='changeTextBoxBorderColor("firstname")' />
   </div>
   <div class="col-lg-6 no-pds">
    <input type="text" value="" id="lastname" name="lastname" placeholder="Last Name" onkeyup='changeTextBoxBorderColor("lastname")'/>
   </div>
   
  </div>
  
  <div class="row">
   <label>Mobile Number</label>
   <div class="col-lg-12 no-pds">
    <input type="number" value="" id="mobile" name="mobile" placeholder="Enter mobile number" onkeyup='changeTextBoxBorderColor("mobile")' />
   </div>
   
  </div>
  
  <div class="row">
   <label>Login Name</label>
   <div class="col-lg-12 no-pds">
    <input type="text" value="" id="username1" name="username" placeholder="Enter Login Name" onkeyup='changeTextBoxBorderColor("username1")'/>
   </div>
   
  </div>
    
  <div class="row">
   <label>Email</label>
   <div class="col-lg-12 no-pds">
    <input type="email" value="" id="email" name="email" placeholder="Enter Email" onkeyup='changeTextBoxBorderColor("email")'/>
   </div>
   
  </div>

  <div class="row">
   <label>Confirm Email</label>
   <div class="col-lg-12 no-pds">
    <input type="email" value="" id="re_email" name="re_mail" placeholder="Re-Enter Email" onkeyup='changeTextBoxBorderColor("re_email")'/>
   </div>
   
  </div>
  
  <div class="row">
   <label>Date of Birth</label>
   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pds">
    <select id="days" name="dob_day">
	<option value="0"> Select Day</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	<option value="8">8</option>
	<option value="9">9</option>
	<option value="10">10</option>
	<option value="11">11</option>
	<option value="12">12</option>
	<option value="13">13</option>
	<option value="14">14</option>
	<option value="15">15</option>
	<option value="16">16</option>
	<option value="17">17</option>
	<option value="18">18</option>
	<option value="19">19</option>
	<option value="20">20</option>
	<option value="21">21</option>
	<option value="22">22</option>
	<option value="23">23</option>
	<option value="24">24</option>
	<option value="25">25</option>
	<option value="26">26</option>
	<option value="27">27</option>
	<option value="28">28</option>
	<option value="29">29</option>
	<option value="30">30</option>
	<option value="31">31</option>
</select>
   </div>
   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pds">
    <select id="months" name="dob_month">
        <option value="0"> Select Month</option>
        <option value="1">January</option>
        <option value="2">Febuary</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
   </select>
   </div>
   
   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pds">
     <select id="years" name="dob_year">
	<option value="0"> Select Year</option>
	<option value="2011">2011</option>
	<option value="2010">2010</option>
	<option value="2009">2009</option>
	<option value="2008">2008</option>
	<option value="2007">2007</option>
	<option value="2006">2006</option>
	<option value="2005">2005</option>
	<option value="2004">2004</option>
	<option value="2003">2003</option>
	<option value="2002">2002</option>
	<option value="2001">2001</option>
	<option value="2000">2000</option>
	<option value="1999">1999</option>
	<option value="1998">1998</option>
	<option value="1997">1997</option>
	<option value="1996">1996</option>
	<option value="1995">1995</option>
	<option value="1994">1994</option>
	<option value="1993">1993</option>
	<option value="1992">1992</option>
	<option value="1991">1991</option>
	<option value="1990">1990</option>
	<option value="1989">1989</option>
	<option value="1988">1988</option>
	<option value="1987">1987</option>
	<option value="1986">1986</option>
	<option value="1985">1985</option>
	<option value="1984">1984</option>
	<option value="1983">1983</option>
	<option value="1982">1982</option>
	<option value="1981">1981</option>
	<option value="1980">1980</option>
	<option value="1979">1979</option>
	<option value="1978">1978</option>
	<option value="1977">1977</option>
	<option value="1976">1976</option>
	<option value="1975">1975</option>
	<option value="1974">1974</option>
	<option value="1973">1973</option>
	<option value="1972">1972</option>
	<option value="1971">1971</option>
	<option value="1970">1970</option>
	<option value="1969">1969</option>
	<option value="1968">1968</option>
	<option value="1967">1967</option>
	<option value="1966">1966</option>
	<option value="1965">1965</option>
	<option value="1964">1964</option>
	<option value="1963">1963</option>
	<option value="1962">1962</option>
	<option value="1961">1961</option>
	<option value="1960">1960</option>
	<option value="1959">1959</option>
</select>
   </div>
   
  </div>
  
  <div class="row">
   <label>Sex</label>
   <div class="col-lg-6 no-pds">
    <select id="gender" name="gender">
	 <option value="M">Male</option>
	 <option value="F">Female</option>
    </select>
   </div>

   
  </div>
  
  <div class="row">
   <label>Create Password</label>
   <div class="col-lg-12 no-pds">
    <input type="password" value="" id="password1" name="password" placeholder="Create Password" onkeyup='changeTextBoxBorderColor("password")'/>
   </div>
   
   
   
  </div>
  
    <div class="row">
      <button  id="signup_form">Sign Up</button>
    </div>
  
 </form>


 
 </div>
 
 

</div>




</div>










<?php include('footer.php'); ?>
