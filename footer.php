

<div class="for-app-promotion">
 <div class="container">
  <div class="row">
   <div class="col-sm-9 col-md-9 col-lg-9">
    <div class="textPR">
     <h2>Find a Doctor Wherever You Are</h2>
     <p>Download the free Better Doctor app for iPhone</p>
    </div>
   </div>
   <div class="col-sm-3 col-md-3 col-lg-3">
    <a href="#">
     <img src="images/appstore.png" alt="app store" />
    </a>
   </div>
  
  </div>
 </div>
</div>
<footer id="main-footer">

<div class="container">
        <div class="row">


            <!--about widget-->
            <div class="col-md-6 col-sm-4">
                <section class="widget animated fadeInLeft ae-animation-fadeInLeft">
                    <h3 class="title">About WatsDoc</h3>
                    <div class="textwidget">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                        <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                            nisl ut aliquip ex ea commodo consequat.</p>
                    </div>
                </section>
            </div>


            <!--general services-->
            <div class="col-md-3 col-sm-4">
                <section class="widget animated fadeInLeft ae-animation-fadeInLeft">
                    <h3 class="title">About Us</h3>
                    <ul>
                        <li>
                            <a href="#">About Us</a>
                        </li>
                        <li>
                            <a href="#">Mobile App</a>
                        </li>
                        <li>
                            <a href="#">Surgical</a>
                        </li>
                        <li>
                            <a href="#">Diagnostic Services</a>
                        </li>
                        <li>
                            <a href="#">For Doctors</a>
                        </li>
                    </ul>
                </section>










            </div>

            <!--recent posts-->
            <div class="col-md-3 col-sm-4">
                <section class="widget animated fadeInLeft ae-animation-fadeInLeft">
                    <h3 class="title">Useful Links</h3>
                    <ul>
                        <li>
                            <a href="#">Terms of Use</a>
                        </li>
                        <li>
                            <a href="#">Privacy</a>
                        </li>
                        <li>
                            <a href="#">Contact Us</a>
                        </li>
                        <li>
                            <a href="#">Careers</a>
                        </li>
                        <li>
                            <a href="#">How to Live a Healthier Life</a>
                        </li>
                    </ul>
                </section>
            </div>
            <!--subscription form-->
            
        </div>
        <div class="footer-bottom">
            <div class="row">
                <div class="col-sm-7">
                    <p>&copy; Copyright 2016. All Rights Reserved by WatsDoc</p>
                </div>
                <!--footer social icons-->
                <div class="col-sm-5 clearfix">
                    <ul class="footer-social-nav">
                        <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="skype:skypeusername?add" target="_blank"><i class="fa fa-skype"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</footer>












<script>
	function DropDown(el) {
				this.dd = el;
				this.placeholder = this.dd.children('span');
				this.opts = this.dd.find('.dropdown a');
				this.val = '';
				this.index = -1;
				this.initEvents();
			}
			DropDown.prototype = {
				initEvents : function() {
					var obj = this;

					obj.dd.on('click', function(event){
						$(this).toggleClass('active');
						return false;
					});

					obj.opts.on('click',function(){
						var opt = $(this);
						obj.val = opt.text();
						obj.index = opt.index();
						obj.placeholder.text(obj.val);
					});
				},
				getValue : function() {
					return this.val;
				},
				getIndex : function() {
					return this.index;
				}
			}

			$(function() {

				var dd = new DropDown( $('#dd') );
                var ww = new DropDown( $('#ww') );


			});
			

$('.gender-filtr ul li button').on('click', function(){
    $('.gender-filtr ul li button.check').removeClass('check');
    $(this).addClass('check');
});

$('.time-filtr ul li button').on('click', function(){
    $('.time-filtr ul li button.check-1').removeClass('check-1');
    $(this).addClass('check-1');
});





</script>

</body>
</html>
