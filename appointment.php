

<?php include('header.php'); ?>
<?php //echo "<pre>"; print_r($_SESSION); echo  "</pre>";?>
<?php 
   if(!isset($_SESSION['wsKey'])) 
    {
   	 ?>
<script>
	   window.location  ='index.php' ;
</script>
<?php 
   }
   /***************** Check Patient login or not **************************/
   if(isset($_SESSION['username']) && !empty($_SESSION['username'])){
   	  $is_Login = 1;
     }else{
   	  $is_Login = 0;	
     }
   /***************** Check Patient login or not **************************/ 
   ?>
<script>
   $(document).ready(function(){
   	$(".appointment_cancel").click(function(){
   		var appointmentId = $(this).data('val');
   		
   				$.ajax({
   					url: "doctors.php",
   					type: 'POST',
   					data: { 'action' : 'appointment_cancel','appointmentId':appointmentId,'wskey':'<?php echo $_SESSION['wsKey'] ?>'},
   					success: function(data) 
   						{
   							if(data=='INFO') {
   							    alert('Appointment cancelled successfully');
   							     location.reload();
   							} else {
   							     alert('Appointment not cancelled');	
   							}
   						}
   				});
   				return false;
   				
   
   		});
		
		
   		
   		
   
   	});
	
	function showOverAllRating(appId){
		$('#overAllRating_'+appId).rating(function(vote, event){
			//alert(vote);
			$('#starRating_'+appId).val(vote);
			
			// write your ajax code here
			// For example;
			// $.get(document.URL, {vote: vote});
		});
	}
	
   	function addReview(isLogin,appId){
   			if(isLogin == 1){
   				$('#addReview_'+appId).modal('show');
				showOverAllRating(appId);
				}else{
   				$('#login-model').modal('show');
   			}
   			
   		}
</script>
<div class="inner-search-bar">
   <div class="container">
      <h1><i class="fa fa-clock-o"></i> Appointments</h1>
   </div>
</div>
<?php 	
     $date  = date('Y-m-d');
     $futuredate =  date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $date) ) ));
   //	http://i2.server125.com:28080/medical-ws/appointment//future_list_for_patient_with_pagination?patientId=ac953c19-6962-42a6-a361-0a79bf159563&selectedDate=2016-03-06&locationId=&pageSize=25&pageNumber=1
   
           $wskey =  	$_SESSION['data']->patient->wsKey ;
           $id =  	$_SESSION['data']->patient->id ;
    
     
   		$loginUrl =  $baseUrl.'appointment/future_list_for_patient_with_pagination';
   		
   		$ch = curl_init();
   		curl_setopt($ch, CURLOPT_URL, $loginUrl);
   		curl_setopt($ch, CURLOPT_POST, 1);
   		curl_setopt($ch, CURLOPT_POSTFIELDS, 'patientId='.$id.'&selectedDate='.$futuredate.'&locationId=&pageSize=25&pageNumber=1' );
   		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
   			"Ws-Key: $wskey",
   		));
   		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   		$storeFut = curl_exec($ch);
   		$dataFuture =  json_decode($storeFut );
   	    //echo  "<pre>";print_r($dataFuture);	echo  "</pre>";
   		
   		
   		$loginUrl =  $baseUrl.'appointment/old_list_for_patient_with_pagination';
   		$ch = curl_init();
   		curl_setopt($ch, CURLOPT_URL, $loginUrl);
   		curl_setopt($ch, CURLOPT_POST, 1);
   		curl_setopt($ch, CURLOPT_POSTFIELDS, 'patientId='.$id.'&selectedDate=&locationId=&pageSize=25&pageNumber=1');
   		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
   			"Ws-Key: $wskey",
   		));
   		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   		$storeOld = curl_exec($ch);
   		$dataOld =  json_decode($storeOld );
   	/*    echo  "<pre>";
   		print_r($dataOld);
   		echo  "</pre>";*/
   		
   		
   		if(isset($_POST['addReviewBtn'])){
			//echo '<pre>'; print_r($_POST); echo '</pre>';
			
			$rRequestAddReview = array();
			$rRequestAddReview = $_POST;
			
			$wsKey = $_SESSION['wsKey'];
			$encodedData = json_encode($rRequestAddReview);
			//echo '<pre>'; print_r($encodedData); echo '</pre>'; die;
			
			$loginUrl = $baseUrl.'review/create';
			$headers= array('Content-Type: application/json',"Ws-Key: $wsKey"); 
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $loginUrl);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData );
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$store = curl_exec($ch);
			
			$dataResponse =  json_decode($store );
			// echo '<pre>'; print_r($dataResponse) ; echo '</pre>';

			curl_close($ch);
		}
   		
   				
   		?>
<div class="outer-user">
   <div class="container">
      <div class="row">
         <div class="col-lg-6">
            <div class="inr-appoints">
               <h3>Future Appointments</h3>
               <table class="table table-bordered">
                  <tr>
                     <th scope="col">Doctor</th>
                     <th scope="col">Reason</th>
                     <th scope="col">Date</th>
                     <th scope="col">Location City</th>
                     <th scope="col">Status</th>
                     <th scope="col">Action</th>
                  </tr>
                  <?php if($dataFuture) {?>
                  <?php foreach($dataFuture as $futureAppoint ) {?>
                  <tr>
                     <td> <?php if(isset($futureAppoint->physicianName)){ echo $futureAppoint->physicianName; }?></td>
                     <td><?php if(isset($futureAppoint->appointmentReason)){ echo $futureAppoint->appointmentReason ; }?></td>
                     <td>   <?php if(isset($futureAppoint->encounterStartDate)){
                        $seconds = $futureAppoint->encounterStartDate / 1000;
                        echo date("d/m/Y", $seconds); 
                       } ?> 
                     </td>
                     <td><?php if(isset($futureAppoint->locationList)) {echo $futureAppoint->locationList[0]->address->city ;} ?></td>
                     <td><?php if(isset($futureAppoint->locationList)) {echo $futureAppoint->status ; }?></td>
                     <td> <a  title="Appointment Details" href="appointment-details.php?appointmentId=<?php echo $futureAppoint->appointmentId ; ?>" style="cursor:pointer;"> <i class="fa fa-eye"></i></a> <?php if($futureAppoint->status !='CANCELLED') {?><a data-val="<?php echo $futureAppoint->appointmentId ; ?>" style="cursor:pointer;" title="Cancel Appointment" class="appointment_cancel"> <i class="fa fa-remove"></i></a> <?php } ?> </td>
                  </tr>
                  <?php }
                     }
                     ?>
               </table>
            </div>
         </div>
         <div class="col-lg-6">
            <div class="inr-appoints">
               <h3>Appointments History</h3>
               <table class="table table-bordered">
                  <tr>
                     <th scope="col">Doctor</th>
                     <th scope="col">Reason</th>
                     <th scope="col">Date</th>
				     <th scope="col">Location City</th>
                     <th scope="col">Review</th>
                  </tr>
                  <?php if($dataOld) {?>
                  <?php foreach($dataOld as $oldAppoint ) { //echo '<pre>'; print_r($oldAppoint);?>
                  <tr>
                     <td> <?php echo $oldAppoint->physicianName; ?></td>
                     <td><?php echo $oldAppoint->appointmentReason ; ?></td>
                     <td>   <?php 
                        $seconds = $oldAppoint->encounterStartDate / 1000;
                        echo date("d/m/Y", $seconds); 
                        ?> 
                     </td>
                      <td><?php if(isset($futureAppoint->locationList)){echo $futureAppoint->locationList[0]->address->city ; }?></td>
                     <td>
                        <?php 
                           $loginUrl = $baseUrl.'review/fetchBlankReviewIntersection';
                           
                           $ch = curl_init();
                             curl_setopt($ch, CURLOPT_URL, $loginUrl);
                             curl_setopt($ch, CURLOPT_POST, 1);
                             curl_setopt($ch, CURLOPT_POSTFIELDS, 'appointmentId='.$oldAppoint->appointmentId);
                             curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                           	"Ws-Key: $wskey "
                           ));
                             curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                             $store = curl_exec($ch);
                             
                             $fFetchReviewInsertion =  json_decode($store);
                           //echo '<pre>'; print_r($fFetchReviewInsertion) ; echo '</pre>';
                             curl_close($ch);
                           ?>
                        <div class="btn-x btn-x-btn">
                           <!--button  type="button" data-toggle="modal" data-target=".modaladd" onclick="addReview();" >Add a Review</button-->
                           <button  type="button" onclick="addReview(<?php echo $is_Login; ?>,'<?php echo $oldAppoint->appointmentId ?>');" >Add a Review</button>
                        </div>
                        <div class="modal fade modaladd in" tabindex="-1" role="dialog" style="display: none;" id="addReview_<?php echo $oldAppoint->appointmentId ?>">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Add Review</h4>
                                 </div>
                                 <div class="modal-body">
                                    <form action="" method="post">
                                       <div class="row addreview">
                                          <div class="col-lg-12">
                                             <label>Subject</label>
                                             <input type="text" value="" name="subject" placeholder="Subject">
                                          </div>
                                       </div>
                                       <div class="row addreview">
										<?php foreach($fFetchReviewInsertion->reviewItemsIntersectionResponseList as $kKey=>$reviewItems){?>
										<script>
										$(document).ready(function(){
										$('#star_<?php echo $oldAppoint->appointmentId;?>_<?php echo $kKey; ?>').rating(function(vote, event){
											//alert(vote);
											$('#reviewScroe_<?php echo $oldAppoint->appointmentId;?>_<?php echo $kKey; ?>').val(vote);
											// write your ajax code here
											// For example;
											// $.get(document.URL, {vote: vote});
										});
										});
										</script>
										<input type="hidden" name="reviewItemsIntersectionResponseList[<?php echo $kKey; ?>][name]" value="<?php echo $reviewItems->name; ?>">
										<input type="hidden" name="reviewItemsIntersectionResponseList[<?php echo $kKey; ?>][score]" id="reviewScroe_<?php echo $oldAppoint->appointmentId;?>_<?php echo $kKey; ?>" value="0">
										<input type="hidden" name="reviewItemsIntersectionResponseList[<?php echo $kKey; ?>][reviewItemId]" value="<?php echo $reviewItems->reviewItemId; ?>">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <label><?php echo $reviewItems->name; ?></label>
											  <section id="star_<?php echo $oldAppoint->appointmentId;?>_<?php echo $kKey; ?>">
												<input type="radio" name="example" class="rating" value="1" />
												<input type="radio" name="example" class="rating" value="2" />
												<input type="radio" name="example" class="rating" value="3" />
												<input type="radio" name="example" class="rating" value="4" />
												<input type="radio" name="example" class="rating" value="5" />
											</section>
											
                                             <!--div class="dc-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                             </div-->
                                          </div>
										  <?php }?>
                                       </div>
                                       <div class="row addreview">
                                          <div class="col-lg-12">
                                             <label>Comments</label>
                                             <textarea name="comments" placeholder="Enter your comments here..."></textarea>
                                          </div>
                                       </div>
                                       <div class="row addreview">
                                          <div class="col-lg-12">
                                             <label>Publish My Name</label>
												<div class="rdio-inp"><label><input  id="publish" type="checkbox"  value="true" name="publish" /> Yes</label></div>
												
                                           </div>
                                       </div>
                                       <div class="row addreview">
                                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                             <label>Overall Rating</label>
											 <input type="hidden" name="starRating" id="starRating_<?php echo $oldAppoint->appointmentId;?>" value="0">
                                             <div class="dc-rating">
                                                <section id="overAllRating_<?php echo $oldAppoint->appointmentId; ?>">
												<input type="radio" name="example" class="rating" value="1" />
												<input type="radio" name="example" class="rating" value="2" />
												<input type="radio" name="example" class="rating" value="3" />
												<input type="radio" name="example" class="rating" value="4" />
												<input type="radio" name="example" class="rating" value="5" />
												<input type="radio" name="example" class="rating" value="6" />
												<input type="radio" name="example" class="rating" value="7" />
												<input type="radio" name="example" class="rating" value="8" />
												<input type="radio" name="example" class="rating" value="9" />
												<input type="radio" name="example" class="rating" value="10" />
											</section>
                                             </div>
                                          </div>
                                       </div>
									   
									   <input type="hidden" name="physicianId" value="<?php echo $oldAppoint->physicianId;?>">
									   <input type="hidden" name="patientId" value="<?php echo $oldAppoint->patientId;?>">
									   <input type="hidden" name="appointmentId" value="<?php echo $oldAppoint->appointmentId;?>">
									
                                       <div class="row addreview">
                                          <div class="col-lg-12">
                                             <button type="submit" name="addReviewBtn" >Submit Review</button>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                              <!-- /.modal-content -->
                           </div>
                           <!-- /.modal-dialog -->
                        </div>
                     </td>
                  </tr>
                  <?php } 
                     }?>
               </table>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include('footer.php'); ?>

