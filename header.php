<?php include('config.php'); ?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>WatsDoc Home</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap-select.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />

<link rel="stylesheet" href="css/rating.css" type="text/css" media="screen" title="Rating CSS">


<script type="text/javascript" src="js/jquery-1.12.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/oauthpopup.js"></script>

<!--script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script-->
<script type="text/javascript" src="js/rating.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css">
  <script src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/custom.js"></script>
<script>
	function addToFav(doc_id,patient_id ,wskey ,isLogin,is_saved){
			if(isLogin == 1 || patient_id !=''){
				if(doc_id == ""){
					alert('Please refesh page , some error');
				}else{
						if(is_saved == 1){  //1 for already saved in fav , 0 for not exist in fav
							var action = 'removeFav';
						}else{
							var action = 'addtofav';
						}
					 	$.ajax({
								url: "doctors.php",
								type: 'POST',
								data: { 'action' : action ,'doc_id' : doc_id ,'patient_id':patient_id ,'wskey':wskey},
								success: function(data) 
									{
										//alert(data);
										
										if(is_saved == 1){  //1 for already saved in fav , 0 for not exist in fav
											is_saved = 0;
											$('#favTag_'+doc_id).attr('onclick','addToFav("'+doc_id+'","'+patient_id+'","'+wskey+'",'+isLogin+','+is_saved+');');
											$('#imgTag_'+doc_id).attr('class','fa fa-heart-o');
											//$('#textTag_'+doc_id).html(data).fadeOut('slow');
											
											
											$("#textTag_"+doc_id).fadeOut('slow',function(){
												$(this).html('(Save)');
											}).fadeIn("slow");
											
											/*setTimeout(function(){ 
												$("#textTag_"+doc_id).fadeOut('slow',function(){
													$(this).html('(Save)').fadeOut('slow');
												}).fadeIn("slow");
												//$('#textTag_'+doc_id).html('(Save)').fadeOut('slow'); 
											}, 1500);*/
											
										}else{
											is_saved = 1;
											$('#favTag_'+doc_id).attr('onclick','addToFav("'+doc_id+'","'+patient_id+'","'+wskey+'",'+isLogin+','+is_saved+');');
											$('#imgTag_'+doc_id).attr('class','fa fa-heart');
											//$('#textTag_'+doc_id).html(data).fadeOut('slow');
											//setTimeout(function(){ $('#textTag_'+doc_id).html('(Remove)').fadeOut('slow'); }, 1500);
											
											$("#textTag_"+doc_id).fadeOut('slow',function(){
												$(this).html('(Remove)');
											}).fadeIn("slow");
											
										}
										
										//location.reload();
									}
							});
							return false;
					 
				}
			}else{
				$('#login-model').modal('show');
			}
			
   }
   function removeFav(doc_id,patient_id ,wskey ,isLogin){
			if(isLogin == 1 || patient_id !=''){
				if(doc_id == ""){
					alert('Please refesh page , some error');
				}else{
					 	$.ajax({
								url: "doctors.php",
								type: 'POST',
								data: { 'action' : 'removeFav' ,'doc_id' : doc_id ,'patient_id':patient_id ,'wskey':wskey},
								success: function(data) 
									{
										alert(data);
									
									}
							});
							return false;
					 
				}
			}else{
				$('#login-model').modal('show');
			}
			
   }
   
$(document).ready(function(){
	
	$("#apt").click(function(){
		<?php if(isset($_SESSION['wsKey'])){ ?>
	           window.location  ='appointment.php' ;
		  <?php }  else { ?>
			  	  $('#login-model').modal('show');
		 <?php } ?> 
	});
	function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };
	$("#form1_btn").click(function(){
	if($.trim($('#username').val()) == ""){
		var username = $("#username");
		$('#username').attr('placeholder','Mobile Number / Email / username');
		$('#username').attr('style','border-color:red');
		return false;
		
	}else if($.trim($('#password').val()) == ""){
		var password = $("#password");
		$('#password').attr('placeholder','Please enter password');
		$('#password').attr('style','border-color:red');
		return false;	
	} else {
		
		
			  var form_data = $("#form1").serialize()+'&'+$.param({  'action' : 'login' });

				$.ajax({
					url: "doctors.php",
					type: 'POST',
					data: form_data,
					success: function(data) 
						{
							var obj = $.parseJSON(data);
							//alert(obj.type);
							
							//console.log(obj); 
							//return false;
						   if(obj.type =='ERROR'){
							   //alert("User doesn't exist in the system");
							   alert(obj.message);
						   }else if(obj.type =='OK'){
							 window.location.href = obj.message;  
						   }else {
							  location.reload();
						   }
						}
				});
				return false;
			
		}
	});
	$("#form1_forgot").click(function(){ 
		//$("#login-model").css("display","none");
		$('#login-model').modal('hide');
		$('#forgotpassword-model').modal('show');
		$('#emailid').attr('placeholder',' Email Id');
	});
	$("#form1_btnforgot").click(function(){
		$('#emailid').val(); 
	if($.trim($('#emailid').val()) == ""){
		var emailId = $("#emailid");
		$('#emailid').attr('placeholder','Please enter Email Id');
		$('#emailid').attr('style','border-color:red');
		return false;
	}else if (!ValidateEmail($('#emailid').val())) {
            var emailId = $("#emailid");
		$('#emailid').attr('placeholder','Please enter Email Id');
		$('#emailid').attr('style','border-color:red');
        return false;
        } else {
		//alert("else part");
		
			 var form_data = $("#form1forgot").serialize()+'&'+$.param({  'action' : 'forgot' });

				$.ajax({
					url: "doctors.php",
					type: 'POST',
					data: form_data,
					success: function(data) 
						{
						   if(data =='ERROR'){
							   alert("User doesn't exist in the system");
						   } else {
							   alert(data);
							  $('#forgotpassword-model').modal('hide');
							  //location.reload();
						   }
						}
				});
				return false;
			
		}
	});
	
	<?php if(isset($_SESSION['wsKey'])){ ?>
	
	$("#logout").click(function(){
		
		$.ajax({
			url: "doctors.php",
			type: 'POST',
			data: { 'action' : 'logout' ,'wsKey' :'<?php echo $_SESSION['wsKey']; ?>' },
			success: function(data) 
				{
				  alert("You have successfully logout");
				  window.location.href = "index.php";
				}
		});
		return false;
	});
	<?php } ?>
	
	$('#facebook').click(function(e){
		$.oauthpopup({
			path: 'Sdk/login.php',
			width:600,
			height:300,
			callback: function(){
				window.location.reload();
			}
		});
		e.preventDefault();
	});
		
});
</script>
</head>

<body>


<!-- Modal -->
<div class="modal fade" id="login-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Log In</h4>
      </div>
      <div class="modal-body">
       <form action="" method="POST" id="form1"> 
        <div class="inp-cover">
         <input type="text" value="" id="username" name="username" placeholder="Mobile Number / Email / username">
        </div>
        
        <div class="inp-cover">
         <input type="password" value="" id="password" name="password" placeholder="Password">
        </div>
       <a href="#" id="form1_forgot" class="forgot">Forgot Password ?</a>
        <div class="inp-cover">
         <button id="form1_btn">Login</button>
        </div>
        <div class="or">
         <span>OR</span>
        </div>
        
        <div class="inp-cover">
         <a href="#" id="facebook" class="login-facebook"><i class="fa fa-facebook-square"></i> Login with Facebook</a>
        </div>
        
        
       </form>
      </div>
      
      <div class="modal-footer">
		   <h4>New to WatsDoc? <a href="sign-up.php<?php if(isset($_GET['physicianid']) && (!empty($_GET['physicianid']))){echo '?physicianid='.$physicianid = $_GET['physicianid'];}else{ echo $physicianid = ""; } ?>">Sign Up</a></h4>
      </div>
      
    </div>
  </div>
</div>
<!-- Forgot Password Modal Start ----->
<div class="modal fade" id="forgotpassword-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
      </div>
      <div class="modal-body">
       <form action="" method="POST" id="form1forgot"> 
        <div class="inp-cover">
         <input type="email" value="" id="emailid" name="email" placeholder="Email ID">
        </div> 
        <div class="inp-cover">
         <button id="form1_btnforgot">Forgot Password</button>
        </div>         
       </form>
      </div>      
      <!--div class="modal-footer">
        <h4>New to WatsDoc? <a href="sign-up.php">Sign Up</a></h4>
      </div-->      
    </div>
  </div>
</div>
<!-- Forgot Password Modal End ----->

<header>
 <div class="top-strip">
  <div class="container">
  <div class="languge">
    <select>
     <option>English</option>
     <option>Arabic</option>
     <option>French</option>
     <option>Spanish</option>
    </select> 
   </div>
  <div class="stip-content">
    
   
   <div class="login-section">
	<?php
	if(!isset($_SESSION['wsKey']) ||  $_SESSION['wsKey'] =='' )  { ?>   
    <a href="#" data-toggle="modal" data-target="#login-model">
     <i class="fa fa-user"></i> Login / Join
    </a>
    <?php } else { ?>
		 
  <a href="profile-settings.php" id="profile-view">   <i class="fa fa-user"></i> (<?php if(isset($_SESSION['data']->patient->name->givenName) && !empty($_SESSION['data']->patient->name->givenName)){
	echo $_SESSION['data']->patient->name->givenName;  
  }else{
	  echo $_SESSION['username'] ;
  }  ?>) </a><a href="#" id="logout"> logout
    </a>
	<?php } ?>	
   </div>
   
   
<!--   <div class="doctor-login">
    <span>DOCTOR</span> : <a href="#">Update your free profile</a>
   </div>-->
   
  </div>
  </div>
 
 </div>
 <div class="menu-section container">
 
   <nav class="navbar navbar-default">
   <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>      <a class="navbar-brand" href="index.php">
       <img src="images/logo.png" alt="Lgoo" />
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php">Home</a></li>
        <li><a href="#">Doctors</a></li>
        <li><a href="#">Department</a></li>
        <li><a id="apt" href="#">Appointment</a></li>
        <li><a href="#">Blogs</a></li>
        <li><a href="#">Contact us</a></li>
        
      </ul>
    </div><!-- /.navbar-collapse -->
</nav>

 </div>

</header>
