<?php include('header.php'); ?>

<div class="intro-section">
<img src="images/slide-1.jpg" class="hm-slide" alt="Slide-1">
 <div class="home-search-fields">
  <div class="container">
   <div class="find-section"> 
    <h2>Find your doctor.Feel better faster.</h2> 
    <h3>+500 verified doctors on WatsDoc</h3>
    
    <div class="home-search-area">
		             <?php 
              
				$loginUrl = $baseUrl.'/speciality/list_guest_visible';

				//init curl
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $loginUrl);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$store = curl_exec($ch);
				
				curl_close($ch);
				$dataSpeciality =  json_decode($store );

              
				$loginUrl = $baseUrl.'/location/list_guest_visible';

				//init curl
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $loginUrl);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$storeLoc = curl_exec($ch);
				
				curl_close($ch);
				$dataLocation=  json_decode($storeLoc );
              // echo  "<pre>";print_r($dataLocation) ;	  echo  "</pre>";
          $dname_list = array();					
   foreach($dataSpeciality as $key=>$row) {
		$dname_list[$key]['key']= $row->id;      
		$dname_list[$key]['value']=$row->name;      
    }
	
	
    $locationname=json_encode($dname_list);
              // echo  "<pre>";print_r($locationname) ;	  echo  "</pre>";
              ?> 
			  
<script>
$(function() {
		//alert('test');
	
		var vatrate = <?php  echo $locationname; ?>;
   
//	alert(vatrate);
    $("#department_name").autocomplete({
        source: vatrate,
        autoFocus:true,
		minLength: 0,     
      focus: function( event, ui ) {
		
        //$( "#department_name" ).val( ui.item.value );
		//$( "#department_name" ).val( ui.item.value );
        //$( "#project" ).val( ui.item.key );
		 $( "#project" ).val(ui.item.key);
		$( "#specil_name" ).val('');
        return false;
      },
      select: function( event, ui ) {
		 
        $( "#department_name" ).val( ui.item.value );
        //$( "#project" ).val( ui.item.key );
		 $( "#project" ).val(ui.item.key);
		$( "#specil_name" ).val('');		
		  
	 /*if(ui.item.key == ""){
		 
		  $( "#project" ).val("");
		 
		  }	else{
			   $( "#project" ).val(ui.item.key);	
				$( "#specil_name" ).val();			   
		  }*/
        return false;
      } 
	  });	
	$("#department_name").keyup(function(){	
			$( "#project" ).val("");
			
		  $( "#specil_name" ).val($("#department_name").val());
	  });	  
	 
	  
});
</script>
      <form  action="search.php" method='GET'>		  
		  <div class="dv-l doc-search"> <i class="fa fa-search"></i>
		  <input id="department_name" placeholder="Choose Speciality" type="text"  size="50" value="" />
		  <input  type="hidden" name="specialityId" id="project" value=""  />
		  <input  type="hidden" name="name" id="specil_name" value="" />		  
          </div>
          
          <div class="dv-l"> 
           <i class="fa fa-map-marker"></i>            
            <select name="locationId" id="basic" class="selectpicker show-tick form-control" data-live-search="true">
				<option value="">Select Location</option>
				 <?php foreach($dataLocation as $location) { ?> 
					 <?php if($location->status == 'ACTIVE') { ?>
						<option value="<?php echo $location->id ; ?>"><?php echo $location->address->city ; ?></option>
				<?php  } } ?>
            </select>
           
          </div>
      
          <div class="bt-cvr">
            <button type="submit">Find a Doctor</button>
          </div>
        </form>


    </div>
    
    
   </div>     
   
   
  
  </div> 
 </div>   


  
             
</div>

<div class="adhe">
 <div class="container">
  <div class="row">
   
   
   <div class="col-sm-4 col-md-4 col-lg-4">
    <div class="ins">
     <div class="md-icn">
      <i class="fa fa-medkit"></i>
     </div>
     <h3>Medical Counseling</h3>
     <p>Sed diam nonummy nibh euismod tincidunt.</p>
    
    </div>
   </div>
   
   <div class="col-sm-4 col-md-4 col-lg-4">
    <div class="ins">
     <div class="md-icn">
      <i class="fa fa-user-md"></i>
     </div>
     <h3>Qualified Doctors</h3>
     <p>Sed diam nonummy nibh euismod tincidunt.</p>
    
    </div>
   </div>
 
   <div class="col-sm-4 col-md-4 col-lg-43">
    <div class="ins">
     <div class="md-icn">
      <i class="fa fa-ambulance"></i>
     </div>
     <h3>Emergency Services</h3>
     <p>Sed diam nonummy nibh euismod tincidunt.</p>
    
    </div>
   </div>
   
   
  </div>
 </div>
</div>


<div class="home-doctors">
 <div class="container">
	   <?php 
  
  	 $loginUrl = $baseUrl. 'physician/search_guest_visible';
		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, 'name=&locationId=&specialityId=');
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'name=&locationId=&specialityId=&gender=&availableToday=&onlineBooking=&homeCall=&sortByConsultationFees=&sortByReviews=&selectedDate=');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$storeDoctors = curl_exec($ch);
	
		curl_close($ch);
	    $dataDoctors =  json_decode($storeDoctors );
	//    echo  "<pre>"; print_r( $dataDoctors ) ; echo  "</pre>";

  ?> 
	 

  <h1>Doctors Across The World</h1>
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet<br /> dolore magna aliquam erat volutpat.</p>
  
	<div class="row">  
		
		<?php
		  $i = 0 ;
		if( !empty($dataDoctors) ){
		  foreach($dataDoctors  as $doctors ) { 
			/*  echo "<pre>";
			  print_r($doctors);*/
			  if($i == 4 ) {
				  break ; 
			  }
			  ?>
		
				 <div class="col-sm-3 col-md-3 col-lg-3 pad-no">
				  
				  <div class="common-doctor">
					  <figure> <a title="Dr.Addison Alexander" href="profile-doc.php?physicianid=<?php echo $doctors->id ; ?>&locationId=<?php echo $doctors->locationList[0]->id ; ?>"> <img  alt="doctor-2" src="<?php echo empty($doctors->image) ? 'images/demouser.jpg' :   $baseUrl .'images/'.$doctors->image ;?>"> </a> </figure>
					  <div class="text-content">
						<h5><a href="profile-doc.php?physicianid=<?php echo $doctors->id ; ?>&locationId=<?php echo $doctors->locationList[0]->id ; ?>"><?php echo $doctors->prefix. ' '. $doctors->givenName.' '.$doctors->middleName ; ?></a></h5>
						<div class="for-border"></div>

<!--
						<p><?php print_r( $doctors->locationList[0]->id) ; ?> </p>
-->

						
						
						<a href="profile-doc.php?physicianid=<?php echo $doctors->id ; ?>&locationId=<?php if(!empty($doctors->defaultLocation->id)){echo $doctors->defaultLocation->id; } else {echo $doctors->locationList[0]->id ;} ?>" class="custom-btn">View Profile</a>
					  </div>
					</div>		  
				</div>
			 
		 <?php 
		 $i++ ;
		 } } ?>

	</div>

 </div>
</div>


<?php include('footer.php');?>
