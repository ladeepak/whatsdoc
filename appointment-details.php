

<?php include('header.php'); ?>
<?php //echo "<pre>"; print_r($_SESSION); echo  "</pre>";?>
<?php 
   if(!isset($_SESSION['wsKey'])) 
    {
   	 ?>
<script>
	   window.location  ='index.php' ;
</script>
 <?php 
   } 
   if(!isset($_REQUEST['appointmentId']) && empty($_REQUEST['appointmentId']) ) 
    {
	 ?>
	<script>
		   window.location  ='appointment.php' ;
	</script>
	<?php 
	}
   ?>
<div class="inner-search-bar">
   <div class="container">
      <h1><i class="fa fa-clock-o"></i> Appointments Details</h1>
   </div>
</div>
<?php 	
          $appointmentId =   $_REQUEST['appointmentId'] ;
           $wskey =  	$_SESSION['data']->patient->wsKey ;
           $id =  	$_SESSION['data']->patient->id ;
    
     
   		$loginUrl =  $baseUrl.'appointment/details';
   		
   		$ch = curl_init();
   		curl_setopt($ch, CURLOPT_URL, $loginUrl);
   		curl_setopt($ch, CURLOPT_POST, 1);
   		curl_setopt($ch, CURLOPT_POSTFIELDS, 'appointmentId='.$appointmentId );
   		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
   			"Ws-Key: $wskey",
   		));
   		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   		$storeFut = curl_exec($ch);
   		$dataAppoint =  json_decode($storeFut );
   	/*   echo  "<pre>";
   		print_r($dataAppoint);
   		echo  "</pre>";*/
   				
   		?>
<div class="outer-user">

<div class="container">
 <div class="doctor-appoitn">
 <?php if ($dataAppoint) { ?>
    <div class="row">
   <label> Doctor Information</label>
   <div class="col-lg-12 no-pds">
	   	  <div class="rd-check">
		<p style="margin-left:30px"> 
			<?php echo $dataAppoint->primaryHandler->name->prefix .' '.$dataAppoint->primaryHandler->name->givenName .' '.$dataAppoint->primaryHandler->name->familyName ;?> 
			<br />
					   <?php  echo $dataAppoint->billedLocation->address->representation ; ?>

			
		   </p>
		   
<!--
		   <p><img alt="doc" src="<?php echo empty($dataAppoint->primaryHandler->image) ?'images/demouser.jpg': $baseUrl .'images/'.$dataAppoint->primaryHandler->image ; ?>"></p>
-->
		    </div>
		  </div>
  </div>
 
  
  <p class="secrue"><i class="fa fa-lock"></i> Secure Booking</p>
  
 </div>
 
 <div class="sign-up">

 <div class="hedings">
  <h5>Appointment Details </h5>
  <p>This will help you manage your appointments</p>
 </div>
  <div class="row">
   <label> Patient Information</label>
   <div class="col-lg-12 no-pds">
		  <div class="rd-check">
      <p style="margin-left:30px"> 
		<?php echo $dataAppoint->patient->name->prefix .' '.$dataAppoint->patient->name->givenName .' '.$dataAppoint->primaryHandler->name->familyName ;?>  
		  </p>  
		  </div>
		   </div>
  </div>
 
  <div class="row">
   <label> Patient Type</label>
   <div class="col-lg-12 no-pds">
	   	  <div class="rd-check">
		<p style="margin-left:30px"> 
			<?php echo $dataAppoint->patient->patientType ;?>  
		   </p>
		    </div>
		  </div>
  </div>
  
  <div class="row">
   <label> Patient Phone Number</label>
   <div class="col-lg-12 no-pds">
	   	   	  <div class="rd-check">
			<p style="margin-left:30px"> 
			<?php echo $dataAppoint->patient->homeTelephone->representation ;?>  
			</p>
			 </div>
			
			   </div>
  </div>
 
  
  <div class="row">
   <label> Appointment Time</label>
   <div class="col-lg-12 no-pds">
	   	   	  <div class="rd-check">
			<p style="margin-left:30px"> 
				<p class="booking-time" style="margin-left:30px">  <?php
 
    
                     $seconds1 = $dataAppoint->encounterStartDate / 1000;
                        echo date("D, F j - g:i a", $seconds1);  ?><i class="fa fa-calendar"></i>
				</p>
			</p>
		 </div>
			
   </div>
  </div>
 

 </div>

<?php } else { ?>
	
	
	Sorry! due some network error we can not process this request please try again.
	
	
<?php	}?>
</div>


</div>

<?php include('footer.php'); ?>

