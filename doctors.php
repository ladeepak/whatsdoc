<?php 
include('config.php');
 
 if($_REQUEST['action']=='store_Action'){
		$_SESSION['postUrl']=$_REQUEST['postUrl'];
	}
	
	if($_REQUEST['action']=='login'){
		$login = $_REQUEST['username'];
		$password = $_REQUEST['password'];
		$loginUrl =  $baseUrl.'login_for_patient_without_encryption';

		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'login='.$login.'&password='.$password.'&mobileDeviceId=&email=&loginFrom=&deviceType=');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$store = curl_exec($ch);

		$data =  json_decode($store );
		
		if($data->type == 'INFO'){
			//print_r($data);
			$_SESSION['wsKey'] = $data->message  ;
			$_SESSION['username'] = $login  ;
			$_SESSION['data'] = $data  ;
		}
		
		if(isset($data->type) && ($data->type == 'INFO')){
			if(isset($_SESSION['postUrl']) && (!empty($_SESSION['postUrl']))){
				//echo $_SESSION['postUrl'];
				$data = (object) array_merge( (array)$data, array('type'=>'OK','message' => $_SESSION['postUrl'] ) );
				print_r(json_encode($data));
			}else if(isset($data->type) && ($data->type == 'ERROR')){
				print_r(json_encode($data)) ;
			}else{
				print_r(json_encode($data)) ;
			}
		}else{
			print_r(json_encode($data));
		}
		
		
		
		
		

		curl_close($ch);
	}
	
	if($_REQUEST['action']=='forgot'){
		$login = $_REQUEST['email'];		
		$loginUrl =  $baseUrl.'patient_forgetPassword';

		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'email='.$login);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$store = curl_exec($ch);

		$data =  json_decode($store );
		print_r($data->message);

		curl_close($ch);
	}
	
    if($_REQUEST['action']=='logout'){
			 
		$wsKey = $_REQUEST['wsKey'];
		$loginUrl =  $baseUrl.'logout';

		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Ws-Key: $wsKey "
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	   $store = curl_exec($ch);

		$data =  json_decode($store );

		print_r($data) ;

        session_destroy();
		curl_close($ch); 
			 
			 
	 }

    if($_REQUEST['action']=='regiter_patient'){
		
		$gender = $_REQUEST['gender'] ;
		$dob_day = $_REQUEST['dob_day'] ;
		$dob_month = $_REQUEST['dob_month'] ;
		$dob_year	 = $_REQUEST['dob_year'] ;
		$email = $_REQUEST['email'] ;
		$firstname = $_REQUEST['firstname'] ;
		$username = $_REQUEST['username'] ;
		$lastname = $_REQUEST['lastname'] ;
		$mobile = $_REQUEST['mobile'] ;
		$re_mail = $_REQUEST['re_mail'] ;
		$password = $_REQUEST['password'] ;
		$dob = $dob_year.'-'. $dob_month .'-'.$dob_day ;
	 
		$loginUrl =  $baseUrl.'register_patient';
	
		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'firstName='.$firstname.'&lastName='.$lastname.'&username='.$username.'&email='.$email.'&mobileNumber='.$mobile.'&password='.$password.'&gender='.$gender.'&dob='.$dob);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $store = curl_exec($ch);
		
		$dataSignUp =  json_decode($store);
		
		//echo '<pre>'; print_r($_SESSION['postUrl']); print_r($store); die;
		if(isset($dataSignUp->response) && ($dataSignUp->response == 'Success')){ //echo 'ENTER1';
			
			if(isset($_SESSION['postUrl']) && (!empty($_SESSION['postUrl']))){ //echo '--ENTER2';
				
				$login = $username;
				$password = $password;
				$loginUrl =  $baseUrl.'login_for_patient_without_encryption';

				//init curl
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $loginUrl);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, 'login='.$login.'&password='.$password.'&mobileDeviceId=&email=&loginFrom=&deviceType=');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$store1 = curl_exec($ch);

				$dataLogin =  json_decode($store1);
				if(isset($dataLogin->type) && ($dataLogin->type == 'INFO')){ //echo '--ENTER3';
					$_SESSION['wsKey'] = $dataLogin->message  ;
					$_SESSION['username'] = $login  ;
					$_SESSION['data'] = $dataLogin  ;
					
					$dataSignUp = (object) array_merge( (array)$dataSignUp, array('response'=>'OK','url' => $_SESSION['postUrl'] ) );
					print_r(json_encode($dataSignUp));
				}
			}else if(isset($dataSignUp->response) && ($dataSignUp->response == 'Error')){ //echo '--ENTER4';
				print_r(json_encode($dataSignUp)) ;
			}else{ //echo '--ENTER5';
				print_r(json_encode($dataSignUp)) ;
			}
		}else{ //echo '--ENTER6';
			print_r(json_encode($dataSignUp)) ;
		}
          
		curl_close($ch); 
			 
			 
	 }
	 
	if($_REQUEST['action']=='addtofav'){
		
		$doc_id = $_REQUEST['doc_id'];
		$patient_id = $_REQUEST['patient_id'];
		$wskey = $_REQUEST['wskey'];
		
		$loginUrl =  $baseUrl.'patient/add_to_favourites';
		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'patientId='.$patient_id.'&physicianId='.$doc_id);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Ws-Key: $wskey"
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$store = curl_exec($ch);

		$data =  json_decode($store );
		print_r($data->message);

		curl_close($ch);
	}
	if($_REQUEST['action']=='removeFav'){
		
		$doc_id = $_REQUEST['doc_id'];
		$patient_id = $_REQUEST['patient_id'];
		$wskey = $_REQUEST['wskey'];
		
		$loginUrl =  $baseUrl.'patient/remove_from_favourites';
		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'patientId='.$patient_id.'&physicianId='.$doc_id);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Ws-Key: $wskey"
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$store = curl_exec($ch);

		$data =  json_decode($store );
		print_r($data->message);

		curl_close($ch);
	}
	
	if($_REQUEST['action']=='appointment_create'){
	    $signupArray = array();	 

		$wskey = $_REQUEST['wskey'] ;
		
		$signupArray['appointmentReason']=$_REQUEST['reason'];	  
		$signupArray['primaryContactNumber']=$_REQUEST['mobile'];	 
		$signupArray['comments']=$_REQUEST['comments'];	 
		
		if(isset($_REQUEST['firstname'])){
			$signupArray['firstName']=$_REQUEST['firstname'];	 
		} else {
			$signupArray['firstName']='';	 

		}
		if(isset($_REQUEST['lastname'])){
			$signupArray['lastname']=$_REQUEST['lastname'];	 
		} else {
			$signupArray['lastname']='';	 

		}
	
		$signupArray['middleName']='';	 
		$signupArray['giveDoctorAccess']=true;	 
		$signupArray['appointmentForRelative']=$_REQUEST['checktype'];	  
		$signupArray['billedLocationId']= $_REQUEST['billedLocationId'];	 
		$signupArray['physicianId']= $_REQUEST['physicianId'];	 
		$signupArray['patientId']= $_REQUEST['patientId'] ;	 
		$signupArray['encounterStartDate']=$_REQUEST['encounterStartDate'];	 
		$signupArray['encounterEndDate']=$_REQUEST['encounterEndDate'];	

		if(isset($_REQUEST['dob_year'])){
			$signupArray['dobRelative']=$_REQUEST['dob_year'].'-'.$_REQUEST['dob_month'].'-'.$_REQUEST['dob_day'];	 
		} else {
			$signupArray['dobRelative']='';	 

		}
		
	    $jsonAppointData = json_encode($signupArray);
	
		
		$loginUrl =  $baseUrl.'appointment/create_by_patient';
		$headers= array('Content-Type: application/json',"Ws-Key: $wskey"); 
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonAppointData );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$store = curl_exec($ch);
        
		$data =  json_decode($store );
		 print_r($data->type) ;

		curl_close($ch);
	}
	
		 
	if($_REQUEST['action']=='appointment_cancel'){
		
		$appointmentId = $_REQUEST['appointmentId'];
		$wskey = $_REQUEST['wskey'];
		
		$loginUrl =  $baseUrl.'appointment/cancel_by_patient';
		//init curl
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'appointmentId='.$appointmentId);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Ws-Key: $wskey"
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$store = curl_exec($ch);

		$data =  json_decode($store );
		print_r($data->type);

		curl_close($ch);
	}
	
  
    if($_REQUEST['action']=='update_profile'){
	
		$gender = $_REQUEST['gender'] ;
		$dob_day = $_REQUEST['dob_day'] ;
		$dob_month = $_REQUEST['dob_month'] ;
		$dob_year	 = $_REQUEST['dob_year'] ;
		$email = $_REQUEST['email'] ;
		$firstname = $_REQUEST['firstname'] ;
		$lastname = $_REQUEST['lastname'] ;
		$mobile = $_REQUEST['mobile'] ;
		$patientId = $_REQUEST['patient_id'] ;
		$wskey = $_REQUEST['wskey'] ;
		$dob = $dob_year.'-'. $dob_month .'-'.$dob_day ;
		/********* For Notification *******************/
		
		$allowedParms = array('pushServiceAllowed_updatePushServiceForPatient'=>'false','emailServiceAllowed_updateEmailNotificationForPatient'=>'false','smsServiceAllowed_updateSmsNotificationForPatient'=>'false');
		
		if(isset($_REQUEST['email_notify']) && $_REQUEST['email_notify'] == 'on'){
			
			$allowedParms['emailServiceAllowed_updateEmailNotificationForPatient'] = 'true';
		}
		
		if(isset($_REQUEST['text_notify']) && $_REQUEST['text_notify'] == 'on'){
			$allowedParms['smsServiceAllowed_updateSmsNotificationForPatient'] = 'true';
		}
		
		if(isset($_REQUEST['push_notify']) && $_REQUEST['push_notify'] == 'on'){
			$allowedParms['pushServiceAllowed_updatePushServiceForPatient'] = 'true';
		}
		error_reporting(0);
		foreach($allowedParms as $kKey=>$allows){
			//echo '<pre>'; print_r($allowedParms); echo '</pre>';
			$pPartion = explode('_',$kKey);
			$loginUrll =  $baseUrl.'patient/'.$pPartion[1];
			//echo '------patientId='.$patientId.'&'.$pPartion[0].'='.$allows;
			//die;
			//init curl
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $loginUrll);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'patientId='.$patientId.'&'.$pPartion[0].'='.$allows);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				"Content-Type: application/x-www-form-urlencoded",
				"Ws-Key: $wskey"
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$store = curl_exec($ch);

			//echo '<pre>'; print_r($store) ; echo '--End'; die;

			curl_close($ch); 
			
		
		}
		
		/********* For Notification *******************/
	 
		$loginUrl =  $baseUrl.'patient/updatePatientDetails';
	  //  echo $baseUrl.'patient/updatePatientDetails?patientId='.$patientId.'&firstName='.$firstname.'&lastName='.$lastname.'&username='.$username.'&email='.$email.'&mobileNumber='.$mobile.'&dob='.$dob.'&gender='.$gender ;
		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'patientId='.$patientId.'&firstName='.$firstname.'&lastName='.$lastname.'&email='.$email.'&mobileNumber='.$mobile.'&dob='.$dob.'&gender='.$gender);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Ws-Key: $wskey"
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $store = curl_exec($ch);

		print_r($store) ;

		curl_close($ch); 
			 
			 
	 }
	
	
    if($_REQUEST['action']=='update_password'){

		$patientId = $_REQUEST['patient_id'] ;
		$wskey = $_REQUEST['wskey'] ;
		$newpassword = $_REQUEST['newpassword'] ;
		$currentpassword = $_REQUEST['currentpassword'] ;
	 
		$loginUrl =  $baseUrl.'changePasswordPatient';
	 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'patientId='.$patientId.'&newPassword='.$newpassword.'&oldPassword='.$currentpassword);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Ws-Key: $wskey"
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $store = curl_exec($ch);

		print_r($store) ;

		curl_close($ch); 
			 
			 
	 }
	
	
