<?php 
   include('header.php'); 
    /***************** Check Patient login or not **************************/
	if(isset($_SESSION['username']) && !empty($_SESSION['username'])){
		  $is_Login = 1;
	  }else{
		  $is_Login = 0;	
	  }
	/***************** Check Patient login or not **************************/
   
   if(isset($_GET['physicianid'])){
   	$physicianid = $_GET['physicianid'];
   }else{
   	//$physicianid = '018857f53998a40e01399935284b0002';
   }
   
   if(isset($_GET['locationId'])){
   	$locationId = $_GET['locationId'];
   }else{
   	$locationId = '';
   }
   
   /*********************** To get the detail for selected physician Start *****************************/
   //$wsKey = $_SESSION['wsKey']; 
   //$loginUrl = 'http://i2.server125.com:28080/medical-ws/physician/details';
   $loginUrl = $baseUrl.'physician/get_by_id_guest_visible';
   
   
   //init curl
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $loginUrl);
   curl_setopt($ch, CURLOPT_POST, 1);
   curl_setopt($ch, CURLOPT_POSTFIELDS, 'physicianId='.$physicianid);
   
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $store = curl_exec($ch);
   
   $data =  json_decode($store);
   	//echo '<pre>'; print_r($data) ; echo '</pre>';
   curl_close($ch);
   
   /*********************** To get the detail for selected physician End *****************************/
   
   
   
   /*********************** To get the Reviews for selected physician Start *****************************/
   
   //$loginUrl = 'http://i2.server125.com:28080/medical-ws/physician/list';
   $loginUrl = $baseUrl.'review/list_for_physician';
   
   //init curl
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $loginUrl);
   curl_setopt($ch, CURLOPT_POST, 1);
   curl_setopt($ch, CURLOPT_POSTFIELDS, 'physicianId='.$physicianid);
   /*curl_setopt($ch, CURLOPT_HTTPHEADER, array(
   			"Ws-Key: $wsKey "
   		));*/
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $store = curl_exec($ch);
   
   $physicianReviewList =  json_decode($store);
   //	echo '<pre>'; print_r($physicianReviewList) ;
   curl_close($ch);
   
   
   
   /*********************** To get the Reviews for selected physician End *****************************/
   
   
   /*********************** To get the speciality & Location Start ****************************************************/
   
   $loginUrl = $baseUrl.'/speciality/list_guest_visible';
   //init curl
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $loginUrl);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $store = curl_exec($ch);
   
   curl_close($ch);
   $dataSpeciality =  json_decode($store );
   
   $loginUrl = $baseUrl.'/location/list_guest_visible';
   //init curl
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $loginUrl);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $storeLoc = curl_exec($ch);				
   curl_close($ch);
   $dataLocation=  json_decode($storeLoc );
   				
   /*********************** To get the speciality & Location End ****************************************************/
   
   /*********************** To get the Scheduler time slot list Start ****************************************************/
   
   $loginUrl = $baseUrl.'scheduler_page/get_weekly_schedule_for_physician_guest_visible';
   //init curl
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $loginUrl);
   curl_setopt($ch, CURLOPT_POST, 1);
   curl_setopt($ch, CURLOPT_POSTFIELDS, 'physicianId='.$physicianid.'&selectedDate='.date('Y-m-d').'&locationId='.$locationId);
   
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $store = curl_exec($ch);
   
   $dataScheduler =  json_decode($store);
   	//echo '<pre>'; print_r($dataScheduler) ; echo '</pre>';
   curl_close($ch);
   		
   /*********************** To get the Scheduler time slot list End ****************************************************/
   
   ?>
   
    <script>
	   function startAppointment(encounterStartDate,encounterEndDate,isLogin){
		var lLocationId = $('#phyLocation').val();
	   //var rReason = $('#reason').val();
	   var rReason = "";
		if(isLogin == 1){
	  
	   if(lLocationId == ""){
	   alert('Please select the location');
	   }/*else if(rReason == ""){
	   alert('Please select the reason');
	   }*/else{	   
	   window.location = "booking.php?encounterStartDate="+encounterStartDate+"&encounterEndDate="+encounterEndDate+"&physicianId=<?php echo $physicianid; ?>&billedLocationId="+lLocationId+"&reason="+rReason+'&physicianName=<?php echo base64_encode($data->prefix.' '.$data->givenName.' '.$data->familyName);?>';	
	   }
		}else{	
			//alert(rReason);
			var postUrl="booking.php?encounterStartDate="+encounterStartDate+"&encounterEndDate="+encounterEndDate+"&physicianId=<?php echo $physicianid; ?>&billedLocationId="+lLocationId+"&reason="+rReason+'&physicianName=<?php echo base64_encode($data->prefix.' '.$data->givenName.' '.$data->familyName);?>';	
				var data = $.param({  'action' : 'store_Action','postUrl':postUrl });
		$.ajax({
			url: "doctors.php",
			type: 'POST',
			data: data ,
			dataType: "json",
			success: function(data) {
				//write code   
			 }
		});
		
		$('#login-model').modal('show');
		//return false;
			
		}		
	   }
	</script>
<div class="inner-search-bar">
   <div class="container">
      <div class="home-search-area">
	  <?php 
           $dname_list = array();					
   foreach($dataSpeciality as $key=>$row) {
		$dname_list[$key]['key']= $row->id;      
		$dname_list[$key]['value']=$row->name;      
    }
	
	
    $locationname=json_encode($dname_list);
              // echo  "<pre>";print_r($locationname) ;	  echo  "</pre>";
              ?> 
			  
<script>
$(function() {
		//alert('test');
	
		var vatrate = <?php  echo $locationname; ?>;
   
//	alert(vatrate);
    $("#department_name").autocomplete({
        source: vatrate,
        autoFocus:true,
		minLength: 0,     
      focus: function( event, ui ) {
		
        //$( "#department_name" ).val( ui.item.value );
		$( "#department_name" ).val( ui.item.value );
        //$( "#project" ).val( ui.item.key );
		 $( "#project" ).val(ui.item.key);
		$( "#specil_name" ).val('');
        return false;
      },
      select: function( event, ui ) {
		 
        $( "#department_name" ).val( ui.item.value );
        //$( "#project" ).val( ui.item.key );
		 $( "#project" ).val(ui.item.key);
		$( "#specil_name" ).val('');		
		  
	 /*if(ui.item.key == ""){
		 
		  $( "#project" ).val("");
		 
		  }	else{
			   $( "#project" ).val(ui.item.key);	
				$( "#specil_name" ).val();			   
		  }*/
        return false;
      } 
	  });	
	$("#department_name").keyup(function(){	
			$( "#project" ).val("");
			
		  $( "#specil_name" ).val($("#department_name").val());
	  });	  
	 
	  
});
</script>
      <form  action="search.php" method='GET'>		  
		  <div class="dv-l doc-search"> <i class="fa fa-search"></i>
		  <input id="department_name" placeholder="Choose Speciality" type="text" style="padding:13px !important;" size="50" value="" />
		  <input  type="hidden" name="specialityId" id="project" value=""  />
		  <input  type="hidden" name="name" id="specil_name" value="" />		  
          </div>
          
          <div class="dv-l"> 
           <i class="fa fa-map-marker"></i>            
            <select name="locationId" id="basic" class="selectpicker show-tick form-control" data-live-search="true">
				<option value="">Select Location</option>
				 <?php foreach($dataLocation as $location) { ?> 
					 <?php if($location->status == 'ACTIVE') { ?>
						<option value="<?php echo $location->id ; ?>"><?php echo $location->address->city ; ?></option>
				<?php  } } ?>
            </select>
           
          </div>
      
          <div class="bt-cvr">
            <button type="submit">Find a Doctor</button>
          </div>
        </form>
      </div>
   </div>
</div>
<div class="doc-intro">
   <div class="doctors-listing container">
      <div class="row">
         <div class="col-sm-3 col-md-3 col-lg-2"  style="padding:0px 2px;">
            <div class="doc-avatar">
               <figure>
                  <?php if(!empty($data->image)){
					 // var_dump(file_exists($baseUrl.'images/'.$data->image));
					 if(file_exists($baseUrl.'images/'.$data->image == true)){?>
						<img alt="Doctor" src="<?php echo $baseUrl.'images/'.$data->image;?>">	
					 <?php
					 }else{ ?>
						 <img alt="Doctor" src="<?php echo 'images/demouser.jpg';?>"> 
					 
                  
					 <?php } }else{ ?>
                  <img alt="Doctor" src="<?php echo 'images/demouser.jpg';?>">
                  <?php } ?>
               </figure>
            </div>
         </div>
         <div class="col-sm-9 col-md-7 col-lg-7" style="padding:0px 2px;">
            <div class="doc-info">
               <h3><?php echo $data->prefix.' '.$data->givenName.' '.$data->familyName;?></h3>
               <!--h3>Dr. Shirin Peters</h3-->
               <h4><?php echo $data->jobTitle;?></h4>
               <!--h4>Primary Care Doctor</h4-->
               <div class="dc-rating">
                  <ul>
                     <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                     </li>
                     <li><a href="#">Read patient reviews</a></li>
                     <li>Practice: <a href="#">City Care Family Practice</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="map" style="height:300px;">
   <div id="map-canvas"></div>
</div>
<div class="profile-detail-doc">
   <div class="container">
      <div class="col-lg-6">
         <div class="tabs-info">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
               <li role="presentation" class="active"><a href="#profile" aria-controls="home" role="tab" data-toggle="tab">Profile</a></li>
               <li role="presentation"><a href="#reviews" aria-controls="profile" role="tab" data-toggle="tab">Reviews</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
               <?php echo $data->description;?>
               <!--div role="tabpanel" class="tab-pane fade in active" id="profile">
                  <ul>
                   <li>
                    <strong>Care Philosophy Statement</strong>
                    <div class="resul-info">
                     No Information
                    </div> 
                   </li>
                   <li>
                    <strong>Started Practice</strong>
                    <div class="resul-info">
                     Dr. Chiruvolu joined the practice in September 2015.
                    </div>
                   </li>
                   <li>
                    <strong>Education</strong>
                    <div class="resul-info">
                     Medical School. Touro University, New York<br>
                     Drexel University, Philadelphia, master's in Medical Science
                    </div>
                   </li>
                   <li>
                    <strong>Board Certification</strong>
                    <div class="resul-info">
                     American Board of Family Medicine
                    </div>
                   </li>
                   <li>
                     <strong>Hospital Affiliation</strong>
                     <div class="resul-info">
                     No Information
                    </div>
                   </li>
                   <li>
                    <strong>Services</strong> 
                    <div class="resul-info">
                     Primary Care Doctor
                    </div> 
                   </li>
                   <li>
                     <strong>Research & Clinical Interest</strong>
                     <div class="resul-info">
                     No Information
                    </div>
                   </li>
                   <li>
                    <strong>Professional Membership</strong>
                    <div class="resul-info">
                     No Information
                    </div>
                   </li>
                   <li>
                    <strong>Achievement & Recognition</strong>
                    <div class="resul-info">
                     No Information
                    </div>
                   </li>
                   <li>
                    <strong>Insurance</strong>
                    <div class="resul-info">
                     No Information
                    </div>
                   </li>
                   <li>
                    <strong>Consultation Fee</strong>
                    <div class="resul-info">
                    <?php echo $data->consultationFee;?>
                    </div>
                   </li>
                   <li>
                     <strong>Home Consultation Fee</strong>
                     <div class="resul-info">
                     <?php echo $data->homeConsultationFee;?>
                    </div>
                   </li>
                   <li>
                    <strong>Languages.</strong>
                    <div class="resul-info">
                     English, Spanish
                    </div>
                   </li>
                  </ul>
                  
                  </div-->
               <div role="tabpanel" class="tab-pane fade" id="reviews">
				<?php foreach($data->reviewList as $review){ ?>
                  <div class="rows">
                     <div class="col-lg-4 no-pds">
                        <div class="rv-time">
                           <strong><?php	
							//echo $review->review->creationTime;
							//February 16, 2016
							//echo $time = date("M d, Y",$review->review->creationTime);
							 $seconds = $review->review->creationTime / 1000;
							echo date("M d, Y", $seconds); 
						   ?></strong>
                           <p>by <?php echo $review->review->reviewedByName;?><br /> <?php if($review->review->verified == 1){ echo '(Verified Patient)';} else{ echo '(Unverified Patient)';}?></p>
                        </div>
                     </div>
                     <div class="col-lg-8 no-pds">
						
                        <div class="row">
						<?php foreach($review->reviewItemDefinitionList as $reviewItemDefinitionList){ //echo '<pre>'; print_r($reviewItemDefinitionList);?>
                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pds">
                              <div class="type-rev">
                                <h5><?php echo $reviewItemDefinitionList->name; ?></h5>
								 <div class="dc-rating">
								<?php 
									$rateval= $reviewItemDefinitionList->score;
									//$rateval=2.5;
									for($i=1;$i<=5;$i++)
									{
										if($rateval>=1)
										{
											echo "<img src='images/star2.png' class='rating-span'>";
											$rateval=$rateval-1;
										}else if($rateval>=0.5){
											echo "<img src='images/star3.png' class='rating-span'>";
											$rateval=$rateval-1;
										}else if($rateval<0.5 && $rateval>0){
											echo "<img src='images/star1.png' class='rating-span'>";
											$rateval=$rateval-1;
										}else if($rateval<=0){
											echo "<img src='images/star1.png' class='rating-span'>";
										}
									}
								?>
                                
                                    <!--i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i-->
                                 </div>
                              </div>
                           </div>
						<?php } ?>
                            <div class="clearfix"></div>
                           <div class="col-lg-12 no-pds rv-content">
                              <p><?php echo $review->review->comments;?></p>
                           </div>
                        </div>
                     </div>
                  </div>
				<?php } ?>
				
                 <!-- /.modal -->
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-5 pul-r">
         <div class="aple-insur">
            <h2>Book an Appointment</h2>
            <form>
               <label>Choose Location</label>
               <!--select>
                  <option>I'll choose my insurance later</option>
                  <option>insurance</option>
                  <option>insurance-1</option>
                  </select-->
				  <?php //echo '<pre>'; print_r($data->locationList); echo '</pre>';?>
               <select name="locationId" id="phyLocation">
                  <!--option value="">Select Location</option-->
                  <?php foreach($data->locationList as $location) { ?> 
                  <?php if($location->status == 'ACTIVE') { ?>
                  <option selected="<?php if($location == $location->id) { echo 'selected';}?>" value="<?php echo $location->id ; ?>"><?php echo $location->address->city ; ?></option>
                  <?php  } } ?>
               </select>
               <!--label>What's the reason for your visit?</label>
               <select name="reason" id="reason">
                  <option value="">Select Reason</option>
                  <option value="OBGYNs">OBGYNs</option>
                  <option value="illness">illness</option>
                  <option value="Pediatricians">Pediatricians</option>
               </select-->
            </form>
           
            <label>Choose a time to book</label>
            <!--p>160 East 32nd Street, #102, New York, NY, 10016</p-->
			
            <div class="appointment-table">
               <input type="hidden" id="prevDateValue" value="<?php echo $wWeek_before = date('Y-m-d',strtotime("previous week Monday"))?>">
               <input type="hidden" id="nextDateValue" value="<?php echo $wWeek_next = date('Y-m-d',strtotime("next week Monday"))?>">
               <div class="prenxt">
                  <button id="previousScheduler" class="arrow-left"><i class="fa fa-arrow-left"></i></button>
				  
                  <button id="nextScheduler" class="arrow-right"><i class="fa fa-arrow-right"></i></button>
               </div>
               <table id="schedulerTable">
                  <tbody>
                     <tr>
                        <?php 
                           foreach($dataScheduler as $scheduler){
                           	$wWeeekDates = explode("-",$scheduler->label);
                           	
                           	$dDatesPart = explode(".",$wWeeekDates[0]);	
                           	$nmonth = date("m", strtotime($dDatesPart[1]));
                           	
                           	
                           	$date_start = trim($dDatesPart[2]).'/'.$nmonth.'/'.$dDatesPart[0];
                           	//$date_start = date ("Y/m/d", strtotime("-1 day", strtotime($date_start)));         
                           	$end_date = date ("Y/m/d", strtotime("+4 day", strtotime($date_start)));
                           
                           	while (strtotime($date_start) <= strtotime($end_date)) {
                           		$date_start = date ("Y/m/d", strtotime("+1 day", strtotime($date_start))); 
                           		$day = date('l', strtotime($date_start));
                           		
                           		$cClassName = strtolower($day);		
                           		echo "<td class='$cClassName'>$day<span><br>$date_start</span></td>" ;
                           	}
                           ?>
                        <?php 
                           }?>
                     </tr>
                     <?php 
                       
                       // echo '<pre>'; print_r($dataScheduler); echo '</pre>';
                        foreach($dataScheduler as $scheduler){
                        	foreach($scheduler->slotList as $sSlotList){ 
                        	  if(!empty($sSlotList->slotTime) && $sSlotList->slotTime != "No Slot"){
                        		
                        		$wWeeekDates = explode("-",$scheduler->label);
                        	
                        		$dDatesPart = explode(".",$wWeeekDates[0]);	
                        		$nmonth = date("m", strtotime($dDatesPart[1]));
                        		
                        		$date_start = trim($dDatesPart[2]).'/'.$nmonth.'/'.$dDatesPart[0];
                        		//$date_start = date ("Y/m/d", strtotime("-1 day", strtotime($date_start)));         
                        		$end_date = date ("Y/m/d", strtotime("+4 day", strtotime($date_start)));
                        	?>
                     <tr>
                        <?php
                           foreach($sSlotList->rowList as $rowList){ //echo '<pre>'; print_r($rowList); echo '</pre>';  
                           	
                           	
                           	$date_start = date ("Y/m/d", strtotime("+1 day", strtotime($date_start))); 
                           	$day = date('l', strtotime($date_start));
                           	
                           	$cClassName = strtolower($day);		
                           	//echo "<td class='$cClassName'>$day<span><br>$date_start</span></td>" ;
                           ?>
                        <td class="<?php echo $cClassName; ?>" id="<?php echo $cClassName; ?>">   
                           <?php 
                              $aAppointmentTime = explode("-",$sSlotList->slotTime);
                              //$sStartDateTime = $date_start.' '.$aAppointmentTime[0];
                              $endTime = strtotime("+30 minutes", strtotime($aAppointmentTime[0]));
                              
                              $encounterStartDate = base64_encode($date_start.' '.$aAppointmentTime[0]);
                              $encounterEndDate = base64_encode($date_start.' '.date('h:i:s A', $endTime));
							  // echo $aAppointmentTime[0].'----'.date('h:i A');
							   $time = date('H:i',strtotime($aAppointmentTime[0]));
							   
							 //  echo $time.' --- '.date('H:i');
							// echo $date_start.' == '.date('Y/m/d');
                              ?>
                           <a 
                              <?php 
							  
							 
                                 if($sSlotList->blocked == true || $date_start < date('Y/m/d') || ($time < date('H:i') && $date_start == date('Y/m/d'))){
                                 	echo $aClass = 'class="not-avail" title="Not Available"'; 
                                 }else if(isset($rowList->objectList[0]->elementValuesMap->second) && ($rowList->objectList[0]->elementValuesMap->second == "BLOCKED" || $rowList->objectList[0]->elementValuesMap->second == "REQUESTED")){
									echo $aClass = 'class="not-avail" title="Not Available"';  
								 }else{
                                 	echo $aClass = ''; 
                                 ?>
                              onclick="startAppointment('<?php echo $encounterStartDate;?>','<?php echo $encounterEndDate;?>',<?php echo $is_Login;?>,'');"	
                              <?php
                                 }
                                 ?>  href="javascript:void(0);"><?php echo $aAppointmentTime[0]; ?></a>
                        </td>
                        <?php
                           //$sSlotList->blocked
                           //$sSlotList->slotTime
                           
                           //$sSlotList->rowList
                           }
                           ?>
                     </tr>
                     <?php
                        }
                        }
                        }
                        ?>
                  </tbody>
               </table>
               <div class="load" style="display: none;"><img alt="" src="images/load.gif"></div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script>
   function initialize() {
    var myLatlng = new google.maps.LatLng(<?php echo $data->homeAddress->latitude; ?>,<?php echo $data->homeAddress->longitude; ?>);
    var mapOptions = {
      zoom:8,
      center: myLatlng,
		scrollwheel: false
   
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    var marker = new google.maps.Marker({
       
        position: myLatlng,
        map: map,
        title: 'Hello World!'
     
    });
   }
   google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php include('footer.php'); ?>
