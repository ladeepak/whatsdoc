function getBaseUrl() {
    try {
        var url = location.href;

        var start = url.indexOf('//');
        if (start < 0)
            start = 0 
        else 
            start = start + 2;

        var end = url.indexOf('/', start);
        if (end < 0) end = url.length - start;

        var baseURL = url.substring(start, end);
        return baseURL;
    }
    catch (arg) {
        return null;
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(document).ready(function() {

	
	/*$("#mobile > ul li a").click(function() {
		$(this).next().slideToggle();
	});
	
	$("span.customSelect").click(function() {
		$("#mobile").slideToggle();
	});
	
	var klik = 1;
	$(".tab-results").click(function() {
		$(".tabs a").removeClass("active");
		$(".filter, .map").hide();
		$(".search-content .appointment, .search-content .bottom, .search-title").show();
		$(this).addClass("active");
	});
	$(".tab-filters").click(function() {
		$(".tabs a").removeClass("active");
		$(".search-content .appointment, .search-content .bottom, .search-title, .map").hide();
		$("div.filter.clearfix").show();
		$(this).addClass("active");
	});
	$(".tab-map").click(function() {
		$(".tabs a").removeClass("active");
		$(".filter, .search-content .appointment, .search-content .bottom, .search-title").hide();
		$(".map, #map").show();
		$(this).addClass("active");
		initialize();
		$("#map").addClass("tab");
		
		if($(".content .info").length) {
			$(".content .info").hide();
		}
		if($(".table").length) {
			$(".table").hide();
		}
		if($(".content .section").length) {
			$(".content .section").hide();
		}
	});
	$(".tab-profile").click(function() {
		$(".section").show();
		$(".table").hide();
		$(".map").hide();
		$(".info").show();
		$(".tabs a").removeClass("active");
		$(this).addClass("active");
	});
	
	$(".tab-calendar").click(function() {
		$(".section").hide();
		$(".table").show();
		$(".map").hide();
		$(".info").hide();
		$(".tabs a").removeClass("active");
		$(this).addClass("active");
	});*/
	
	/*$("td.arrow-right, .arrow-left").on("click", function() {
	
		obj = $(this);
		
		obj.closest(".appointment-table").children(".load").show();
		
		setTimeout(function() {
			obj.parent().next().find("a").hide();
			obj.parent().next().find("td.tuesday").html('<a href="#">05:00 pm</a><br />'+
												  '<a href="#">05:30 pm</a><br />'+
												  '<a href="#">06:00 pm</a><br />'+
												  '<a href="#">06:30 pm</a><br />'+
												  '<a href="#">07:00 pm</a><br />'+
												  '<a href="#">07:30 pm</a><br />'+
												  '<a href="#">08:00 pm</a><br />'+
												  '<a href="#">more</a>');
												  
			  obj.parent().next().find("td.wednesday").html('<a href="#">05:00 pm</a><br />'+
			  '<a href="#">05:30 pm</a><br />'+
			  '<a href="#">06:00 pm</a><br />'+
			  '<a href="#">06:30 pm</a><br />'+
			  '<a href="#">07:00 pm</a><br />'+
			  '<a href="#">07:30 pm</a><br />'+
			  '<a href="#">08:00 pm</a><br />'+
			  '<a href="#">more</a>');
			obj.parent().next().find("td.friday").html('<a href="#">05:00 pm</a><br />'+
												'<a href="#">05:30 pm</a><br />'+
												'<a href="#">06:00 pm</a><br />'+
												'<a href="#">06:30 pm</a><br />'+
												'<a href="#">07:00 pm</a><br />'+
												'<a href="#">07:30 pm</a><br />'+
												'<a href="#">08:00 pm</a><br />'+
												'<a href="#">more</a>');
			$(".load").hide();
		}, 2000);
		
	});
	
	*/
	
	
	$("#phyLocation").on("change", function() {  
		$(".load").show();
		var physicianid = getParameterByName('physicianid');
		//var locationId = getParameterByName('locationId');
		var locationId = $("#phyLocation").val();
		var d = new Date();

		var month = d.getMonth()+1;
		var day = d.getDate();

		var selectedDate = d.getFullYear() + '-' +
			(month<10 ? '0' : '') + month + '-' +
			(day<10 ? '0' : '') + day;
		setTimeout(function() {
			
			$.ajax({
				type :  'POST',
				//url :   'http://localhost/insonix/doctors/get_scheduler.php?physicianid='+physicianid+'&locationId='+locationId+'&selectedDate='+selectedDate,
				url :   'http://localhost/insonix/doctors/get_scheduler.php?physicianid='+physicianid+'&locationId='+locationId+'&selectedDate='+selectedDate,
				beforeSend: function() {
					$('#schedulerTable').html('');
				},
				error: function() {
					$('#schedulerTable').html(oldScheduler);
				},
				success : function(response){
					//alert(data);
					responseArray = response.split(':::^');
					
					$('#schedulerTable').html(responseArray[2]);
					$('#prevDateValue').val(responseArray[0]);
					$('#nextDateValue').val(responseArray[1]);
				}
			});
			$(".load").hide();
		}, 2000);
		
	});
	
	$("#previousScheduler").on("click", function() {  
		$(".load").show();
		var physicianid = getParameterByName('physicianid');
		//var locationId = getParameterByName('locationId');
		var locationId = $("#phyLocation").val();
		var d = new Date();

		var month = d.getMonth()+1;
		var day = d.getDate();

		var selectedDate = $('#prevDateValue').val();
		

		setTimeout(function() {
			
			$.ajax({
				type :  'POST',
				//url :   'http://localhost/insonix/doctors/get_scheduler.php?physicianid='+physicianid+'&locationId='+locationId+'&selectedDate='+selectedDate,
				url :   'http://localhost/insonix/doctors/get_scheduler.php?physicianid='+physicianid+'&locationId='+locationId+'&selectedDate='+selectedDate,
				beforeSend: function() {
					$('#schedulerTable').html('');
				},
				error: function() {
					$('#schedulerTable').html(oldScheduler);
				},
				success : function(response){
					//alert(data);
					responseArray = response.split(':::^');
					
					$('#schedulerTable').html(responseArray[2]);
					$('#prevDateValue').val(responseArray[0]);
					$('#nextDateValue').val(responseArray[1]);
				}
			});
			$(".load").hide();
		}, 2000);
		
	});
	
	$("#nextScheduler").on("click", function() {  
		$(".load").show();
		var physicianid = getParameterByName('physicianid');
		//var locationId = getParameterByName('locationId');
		var locationId = $("#phyLocation").val();
		var d = new Date();

		var month = d.getMonth()+1;
		var day = d.getDate();

		var selectedDate = $('#nextDateValue').val();
		

		setTimeout(function() {
			var oldScheduler = $('#schedulerTable').html();
			
			$.ajax({
				type :  'POST',
				//url :   'http://localhost/insonix/doctors/get_scheduler.php?physicianid='+physicianid+'&locationId='+locationId+'&selectedDate='+selectedDate,
				url :   'http://localhost/insonix/doctors/get_scheduler.php?physicianid='+physicianid+'&locationId='+locationId+'&selectedDate='+selectedDate,
				beforeSend: function() {
					$('#schedulerTable').html('');
				},
				error: function() {
					$('#schedulerTable').html(oldScheduler);
				},
				success : function(response){
					//alert(data);
					responseArray = response.split(':::^');
					 
					$('#schedulerTable').html(responseArray[2]);
					$('#prevDateValue').val(responseArray[0]);
					$('#nextDateValue').val(responseArray[1]);
				}
			});
			$(".load").hide();
		}, 2000);
		
	});
	
	
	/*$(".mapbtn").click(function() {
		initialize();
		
		if(klik == 1) {
			$("#map").show();
			initialize();
			klik = 0;
			$(this).html("Hide Map");
			$(this).addClass("active");
			$(this).css({'border-radius':'4px 4px 0 0'});
		
			if($(".image-box img").length) {
				$('.image-box img').css({margin:'-60px 0 0 0'});
			}
			$('#breadcrumbs').css({marginBottom:'0'});
		} else {
			$("#map").hide();
			klik = 1;
			$(this).html("Show Map");
			$(this).removeClass("active");
			$(this).css({'border-radius':'4px'});
			
			if($(".image-box img").length) {
				$('.image-box img').css({margin:'40px 0 0 0'});
			}
			$('#breadcrumbs').css({marginBottom:'14px'});
		}
	});
	
	$("a.step").click(function() {
		$(this).parent().hide();
		$(this).parent().next().show();	
	});
	
	$(".one-half.right a.btn:eq(0)").click(function() {
		$("#payment > div").hide();
		$("#login").show();
	});
	
	$(".one-half.right a.btn:eq(1)").click(function() {
		$("#payment > div").hide();
		$("#register").show();
	});
	
	$(".select-style").click(function() {
		$(this).find("ul").slideToggle(200);
	});
	
	$(".select-style ul li").click(function() {
		$(this).parent().parent().find("span").html($(this).find("a").html());
	});
	
	$(".select-style ul").hover(function() {
		$(this).show();
	}, function() {
		$(this).hide();
	});
	
	$("a.next").click(function() {
		$(this).parent().hide();
		$(this).parent().next().show();
	});
	
	$(".tips .btn:eq(1)").click(function() {
		$(this).parent().hide();
	});
	
	$(".section-box h2").click(function() {
		$(this).next().slideToggle();
	});
	
	if($(".fancyb").length) {
		$(".fancyb").fancybox();
	}
	
	if($(".book").length) {
		$(".book").click(function() {
			$('html, body').animate({scrollTop:$('.table').offset().top});
		});
	}
	
	$("a").click(function(e) {
		if($(this).attr("href") == "#") {
			e.preventDefault();
		}
	});
	
	if($("a.map-btn").length) {
		$("a.map-btn").click(function() {
			$(".map").slideToggle();
		});
	}
	
	if($("#nav").length) {	
		$("#nav").superfish({delay:0});
	}
		
	$("a.appointment-btn, .appointment-table a").fancybox();
	
	$("#content .video").click(function() {
		$.fancybox(
			{href : $(this).children("img").attr("alt"), type:'iframe', width: 640, height: 400}
		);
	});*/
	
});




