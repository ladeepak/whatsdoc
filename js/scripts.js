$(document).ready(function(){
	$("#signup_form").click(function(){
	
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	$("#firstname").focusout(function(){
	     var dest = $(this);
        dest.val(dest.val().split(" ").join(""));  
	});
	
	$("#username").focusout(function(){
	     var dest = $(this);
        dest.val(dest.val().split(" ").join(""));  
	});
	
	if($.trim($('#firstname').val()) == ""){
		var firstname = $("#firstname");
		$('#firstname').attr('placeholder','Please enter firstname');
		$('#firstname').attr('style','border-color:red');
		ScrollToTop(firstname);
		return false;
		
	}else if($.trim($('#lastname').val()) == ""){
		var lastname = $("#lastname");
		$('#lastname').attr('placeholder','Please enter lastname');
		$('#lastname').attr('style','border-color:red');
		ScrollToTop(lastname);
		return false;
		
	}else if($.trim($('#mobile').val()) == ""){
		var mobile = $("#mobile");
		$('#mobile').attr('placeholder','Please enter mobile');
		$('#mobile').attr('style','border-color:red');
		ScrollToTop(mobile);
		return false;
		
	}else if($.trim($('#mobile').val().length) < 10 || $.trim($('#mobile').val().length) > 10){
		$('#mobile').val('');
		var mobile = $("#mobile");
		$('#mobile').attr('placeholder','Please enter 10 digits');
		$('#mobile').attr('style','border-color:red');
		ScrollToTop(mobile);
		return false;
		
	}else if($.trim($('#username1').val()) == ""){
		var username = $("#username1");
		$('#username1').attr('placeholder','Please enter username');
		$('#username1').attr('style','border-color:red');
		ScrollToTop(username);
		return false;
		
	}else if($.trim($('#email').val()) == ""){
		var email = $("#email");
		$('#email').attr('placeholder','Please enter email');
		$('#email').attr('style','border-color:red');
		ScrollToTop(email);
		return false;
		
	}else if(!regex.test($.trim($('#email').val()))){
		var email = $("#email");
		$('#email').val('');
		$('#email').attr('placeholder','Please enter a valid email');
		$('#email').attr('style','border-color:red');
		ScrollToTop(email);
		return false;
		
	}else if($.trim($('#re_email').val()) == ""){
		var re_email = $("#re_email");
		$('#re_email').attr('placeholder','Please re-enter email');
		$('#re_email').attr('style','border-color:red');
		ScrollToTop(re_email);
		return false;
		
	}else if($('#email').val() != $('#re_email').val()){
		var email = $("#email");
		$('#email').val('');
		$('#re_email').val('');
		$('#email').attr('placeholder','Emails does not match');
		$('#email').attr('style','border-color:red');
		$('#re_email').attr('style','border-color:red');
		ScrollToTop(email);
		return false;
		
	}
	else if($.trim($('#days').val()) == 0){
		var days = $("#days");
		$('#days').attr('style','border-color:red;');
		ScrollToTop(days);
		return false;
		
	}else if($.trim($('#months').val()) == 0){
		var months = $("#months");
		$('#months').attr('style','border-color:red;');
		ScrollToTop(months);
		return false;
		
	}else if($.trim($('#years').val()) == 0){
		var months = $("#years");
		$('#years').attr('style','border-color:red;');
		ScrollToTop(years);
		return false;
		
	}else if($.trim($('#password1').val()) == ""){
		var password = $("#password1");
		$('#password1').attr('placeholder','Please enter password');
		$('#password1').attr('style','border-color:red');
		ScrollToTop(password);
		return false;
		
	}
	else if($.trim($('#password1').val().length) <= 5 ){
		var password = $("#password1");
		$('#password1').val('');
		$('#password1').attr('placeholder','Your password must be at least 6 characters long');
		$('#password1').attr('style','border-color:red');
		ScrollToTop(password);
		return false;
		
	}	
		
		
		
		
		var data = $('#form_signup').serialize()+'&'+$.param({  'action' : 'regiter_patient' });
		$.ajax({
			url: "doctors.php",
			type: 'POST',
			data: data ,
			dataType: "json",
			success: function(data) {
				   if(data.response =='Success'){
					   alert('You have successfully registered');
				    	window.location  ='index.php' ;		
				   }else if(data.response == 'OK'){
					   alert('You have successfully registered');
					   window.location.href = data.url;  
				   }else if(data.response =='Error') {
					    alert(data.message);
				   } else {
					    alert('There is some problem in system,please try later.');
				   }
			   }
		});
		return false;
	});
});


/*
* Name : ScrollToTop
* Description : Use to focus on validate field.
*/
function ScrollToTop(el) { 
    $('html, body').animate({ 
        scrollTop: $(el).offset().top - 50 }, 
    'slow', function() {
        $(el).focus();
    });           
}//end here

/*
* Name : changeTextBoxBorderColor
* Description : Use to change the border color onkeyup.
*/
function  changeTextBoxBorderColor(id){ 
	document.getElementById(id).style.borderColor = '#d5d5d5'; 
}//end here
