<?php include('header.php'); 
?>
<?php 
if(!isset($_SESSION['wsKey'])) 
 {
	 ?>
	 <script>
	 window.location  ='index.php' ;
	 </script>
	 <?php 
 } 
?>
<script>

$(document).ready(function(){
	
	$(".checktype").click(function(){
		if( $('input[name=checktype]:checked').val() == 'false') {
			$('#some').css('display','block') ;
		} else {
			$('#some').css('display','none') ;
		}
		
	}); 
	
	$("#appointment_create").click(function(){
		
	if( $('input[name=checktype]:checked').val() == 'false') {
		
		if($.trim($('#firstname').val()) == ""){
			var firstname = $("#firstname");
			$('#firstname').attr('placeholder','Please enter firstname');
			$('#firstname').attr('style','border-color:red');
			ScrollToTop(firstname);
			return false;
			
		}else if($.trim($('#lastname').val()) == ""){
			var lastname = $("#lastname");
			$('#lastname').attr('placeholder','Please enter lastname');
			$('#lastname').attr('style','border-color:red');
			ScrollToTop(lastname);
			return false;
			
		}else if($.trim($('#dob_day').val()) == 0){
			var days = $("#dob_day");
			$('#dob_day').attr('style','border-color:red;');
			ScrollToTop(days);
			return false;
			
		}else if($.trim($('#dob_month').val()) == 0){
			var months = $("#dob_month");
			$('#dob_month').attr('style','border-color:red;');
			ScrollToTop(months);
			return false;
			
		}else if($.trim($('#dob_year').val()) == 0){
			var years = $("#dob_year");
			$('#dob_year').attr('style','border-color:red;');
			ScrollToTop(years);
			return false;
			
		} else if($.trim($('#mobile').val()) == ''){
			var months = $("#mobile");
			$('#mobile').attr('style','border-color:red;');
			$('#mobile').attr('placeholder','Please enter mobile');
			ScrollToTop(months);
			return false;
			
		} else {	
			  var form_data = $("#appointment").serialize()+'&'+$.param({  'action' : 'appointment_create' });

				$.ajax({
					url: "doctors.php",
					type: 'POST',
					data: form_data,
					success: function(data) 
						{
							if(data=='INFO') {
							alert('Appointment was created successfully');
							 window.location  ='appointment.php' ;
							} else {
							alert('Appointment not created');	
							}
						}
				});
				return false;
				
			}
	} else if($.trim($('#mobile').val()) == ''){
			var months = $("#mobile");
			$('#mobile').attr('style','border-color:red;');
			$('#mobile').attr('placeholder','Please enter mobile');
			ScrollToTop(months);
			return false;
			
	} else if($.trim($('#mobile').val().length) < 10 | $.trim($('#mobile').val().length) > 10 ){
			$('#mobile').val('');
			var months = $("#mobile");
			$('#mobile').attr('style','border-color:red;');
			$('#mobile').attr('placeholder','Please enter 10 digits ');
			ScrollToTop(months);
			return false;
			
	} else {	
			  var form_data = $("#appointment").serialize()+'&'+$.param({  'action' : 'appointment_create' });

				$.ajax({
					url: "doctors.php",
					type: 'POST',
					data: form_data,
					success: function(data) 
						{
							//alert(data); return false;
							if(data=='INFO') {
							alert('Appointment was created successfully');
							 window.location  ='appointment.php' ;
							} else {
							alert('Appointment not created');	
							}
						}
				});
				return false;
				
			}
		});

	});
</script>
<div class="inner-search-bar">
 <div class="container">
  
  <h1>Appointment</h1>
  
  
 </div>
</div>

<div class="outer-user">

<div class="container">
 <div class="doctor-appoitn">
  <div class="doc-names">
   <table width="100%" border="0">
  <tr>
    <td>
     <img src="images/doctor-av.jpg" alt="doc" />
    </td>
    <td valign="top">
     <h4><?php echo base64_decode($_REQUEST['physicianName']) ; ?></h4>
     <div class="dc-rating">
      <i class="fa fa-star"></i>
      <i class="fa fa-star"></i>
      <i class="fa fa-star"></i>
      <i class="fa fa-star"></i>
      <i class="fa fa-star-half-o"></i>
     </div>
     <?php
        $wskey = $_SESSION['wsKey'] ;
     	$loginUrl =  $baseUrl.'location/details';
		//init curl
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'locationId='.$_REQUEST['billedLocationId']);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Ws-Key: $wskey"
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$store = curl_exec($ch);

		$data =  json_decode($store );
		if(isset($data->address->representation)) {
			$address =  $data->address->representation ;
		} else {
			$address = '';
		}

		curl_close($ch); 
		
		
		?>
     <p> <?php echo $address ;?></p>

    </td>
  </tr>
</table>
    
    <h5><?php echo $startdate =  date('D, F j - g:i a' ,strtotime(base64_decode($_REQUEST['encounterStartDate']))) ; ?> </h5>
    <p>Patient</p>
    <span>TBD</span>
    <p>Reason for Visit</p>
    <span><?php echo $_REQUEST['reason'] ; ?></span>
  
  </div>
  
  <p class="secrue"><i class="fa fa-lock"></i> Secure Booking</p>
  
 </div>
 
 <div class="sign-up">

 <div class="hedings">
  <h5>Book Your Appointment</h5>
  <p>This will help you manage your appointments</p>
 </div>
 <form  action="" method="POST" id="appointment">
  <div class="row">
   <label>What's the reason for your visit?</label>
   <div class="col-lg-12 no-pds">
    <input type="text" value="<?php echo $_REQUEST['reason'] ; ?>"  id="reason" name="reason" placeholder="illness" />


    <input type="hidden" value="<?php echo $_SESSION['wsKey'] ; ?>" id="reason" name="wskey"  />
    <input type="hidden" value="<?php echo date('Y-m-d H:i' ,strtotime(base64_decode($_REQUEST['encounterStartDate']))) ; ?>" id="encounterStartDate" name="encounterStartDate"  />
    <input type="hidden" value="<?php echo date('Y-m-d H:i' ,strtotime(base64_decode($_REQUEST['encounterEndDate']))) ; ?>" id="encounterEndDate" name="encounterEndDate"  />
    <input type="hidden" value="<?php echo $_REQUEST['billedLocationId'] ; ?>" id="billedLocationId" name="billedLocationId"  />
    <input type="hidden" value="<?php echo $_REQUEST['physicianId'] ; ?>" id="physicianId" name="physicianId"  />
    <input type="hidden" value="<?php echo $_SESSION['data']->patient->id ; ?>" id="patientId" name="patientId"  />
   </div>
   
  </div>
  
  <div class="row">
   <label>What do you like to inform your doctor?</label>
   <div class="col-lg-12 no-pds">
 
        <input  id="inform" type="hidden" checked="checked" value="true" name="inform" />
		<input type="text" id="comments" name="comments" /> 

   </div>
   
  </div>
  
  <div class="row">
   <label>Who will be seeing the doctor?</label>
   <div class="col-lg-12 no-pds">
   <div class="rd-check">
    <div class="rdio-inp"><label><input  class="checktype" type="radio" checked="checked" value="true" name="checktype" /> Me</label></div>
    <div class="rdio-inp"><label><input class="checktype" type="radio" value="false"  name="checktype" /> Someone else</label></div>
   </div>
   </div>
   
  </div>
  
  <div id="some" style="display:none">

	  <div class="row">
	   <label>Relative's Name</label>
	   <div class="col-lg-6 no-pds">
		<input type="text" value="" id="firstname" name="firstname" placeholder="First Name" />
	   </div>
	   <div class="col-lg-6 no-pds">
		<input type="text" value="" id="lastname" name="lastname" placeholder="Last Name" />
	   </div>
	   
	  </div>
	  
	  <div class="row">
	   <label>Relative's Date of Birth</label>
	   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pds">
		<select id="dob_day" name="dob_day">
		<option value="0">-</option>
		<option value="01">1</option>
		<option value="02">2</option>
		<option value="03">3</option>
		<option value="04">4</option>
		<option value="05">5</option>
		<option value="06">6</option>
		<option value="07">7</option>
		<option value="08">8</option>
		<option value="09">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
		<option value="31">31</option>
	</select>
	   </div>
	   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pds">
		<select id="dob_month" name="dob_month">
			<option value="0">-</option>
			<option value="01">January</option>
			<option value="02">Febuary</option>
			<option value="03">March</option>
			<option value="04">April</option>
			<option value="05">May</option>
			<option value="06">June</option>
			<option value="07">July</option>
			<option value="08">August</option>
			<option value="09">September</option>
			<option value="10">October</option>
			<option value="11">November</option>
			<option value="12">December</option>
	   </select>
	   </div>
	   
	   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pds">
		 <select id="dob_year" name="dob_year">
		<option value="0">-</option>
		<option value="2011">2011</option>
		<option value="2010">2010</option>
		<option value="2009">2009</option>
		<option value="2008">2008</option>
		<option value="2007">2007</option>
		<option value="2006">2006</option>
		<option value="2005">2005</option>
		<option value="2004">2004</option>
		<option value="2003">2003</option>
		<option value="2002">2002</option>
		<option value="2001">2001</option>
		<option value="2000">2000</option>
		<option value="1999">1999</option>
		<option value="1998">1998</option>
		<option value="1997">1997</option>
		<option value="1996">1996</option>
		<option value="1995">1995</option>
		<option value="1994">1994</option>
		<option value="1993">1993</option>
		<option value="1992">1992</option>
		<option value="1991">1991</option>
		<option value="1990">1990</option>
		<option value="1989">1989</option>
		<option value="1988">1988</option>
		<option value="1987">1987</option>
		<option value="1986">1986</option>
		<option value="1985">1985</option>
		<option value="1984">1984</option>
		<option value="1983">1983</option>
		<option value="1982">1982</option>
		<option value="1981">1981</option>
		<option value="1980">1980</option>
		<option value="1979">1979</option>
		<option value="1978">1978</option>
		<option value="1977">1977</option>
		<option value="1976">1976</option>
		<option value="1975">1975</option>
		<option value="1974">1974</option>
		<option value="1973">1973</option>
		<option value="1972">1972</option>
		<option value="1971">1971</option>
		<option value="1970">1970</option>
		<option value="1969">1969</option>
		<option value="1968">1968</option>
		<option value="1967">1967</option>
		<option value="1966">1966</option>
		<option value="1965">1965</option>
		<option value="1964">1964</option>
		<option value="1963">1963</option>
		<option value="1962">1962</option>
		<option value="1961">1961</option>
		<option value="1960">1960</option>
		<option value="1959">1959</option>
	</select>
	   </div>
	   
	  </div>
	  
	  <div class="row">
	   <label>Relative's Gender</label>
	   <div class="col-lg-12 no-pds">
	   <div class="rd-check">
		<div class="rdio-inp"><label><input type="radio" value="M"  id="gender" name="gender" /> Male</label></div>
		<div class="rdio-inp"><label><input type="radio" value="F" name="gender" /> Female</label></div>
	   </div>
	   </div>
	   
	  </div>
	  
	  <div class="row">
   <label>Are you parent?</label>
   <div class="col-lg-12 no-pds">
   
    <div class="rd-check">
    <div class="rdio-inp"><label><input type="radio" value="" id="parents" name="parent" /> No</label></div>
    <div class="rdio-inp"><label><input type="radio" value="" checked="checked" name="parent" /> Yes</label></div>
   </div>
   
   </div>
   
  </div>
	  
  </div>
  
  
  
  <div class="row">
   <label>Phone number where the doctor can contact you</label>
   <div class="col-lg-12 no-pds">
    <input type="number" value="" name="mobile" id="mobile" placeholder="+91 7307233292" />
   </div>
   
  </div>
  
  <div class="row">
   <label>Appointment time</label>
   <div class="col-lg-12 no-pds">
	<p class="booking-time"><?php echo $startdate =  date('D, F j - g:i a' ,strtotime(base64_decode($_REQUEST['encounterStartDate']))) ; ?> <i class="fa fa-calendar"></i>
	</p>
	<p class="booking-time"><?php echo $enddate =  date('D, F j - g:i a' ,strtotime(base64_decode($_REQUEST['encounterEndDate']))) ; ?> <i class="fa fa-calendar"></i>
	</p>
   
<!--
    <p class="booking-time">Thursday, February 25-9:45 AM <i class="fa fa-calendar"></i></p>
-->
   </div>
   
  </div>

  
    <div class="row">
      <button id="appointment_create" >Confirm Appointment</button>
    </div>
  
 </form>

 </div>


</div>


</div>


<?php include('footer.php'); ?>
